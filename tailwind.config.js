import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';
import aspectratio from '@tailwindcss/aspect-ratio';
import colors from 'tailwindcss/colors';

/** @type {import('tailwindcss').Config} */
export default {
    darkMode: 'class',

    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/**/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Albert Sans', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                primary: {
                    '50': '#f4f8fa',
                    '100': '#e6eff3',
                    '200': '#d2e2eb',
                    '300': '#b3d0dd',
                    '400': '#8fb6cb',
                    '500': '#749fbd',
                    '600': '#5d86ac',
                    '700': '#56789f',
                    '800': '#4a6483',
                    '900': '#3f5369',
                    '950': '#2a3441',
                },
                secondary: {
                    '50': '#f8f6f9',
                    '100': '#f4eef5',
                    '200': '#ebdeec',
                    '300': '#dcc3de',
                    '400': '#c79dc9',
                    '500': '#b680b7',
                    '600': '#a0629f',
                    '700': '#874f83',
                    '800': '#71436e',
                    '900': '#603b5d',
                    '950': '#381f36',
                },
                tertiary: {
                    '50': '#f4f9f5',
                    '100': '#e5f3e9',
                    '200': '#cbe7d3',
                    '300': '#a3d2b1',
                    '400': '#72b686',
                    '500': '#55a56d',
                    '600': '#3d7c50',
                    '700': '#326341',
                    '800': '#2c4f37',
                    '900': '#25422f',
                    '950': '#102317',
                },
                success: colors.green,
                danger: colors.red,
                info: colors.sky,
                warning: colors.amber,
                status: colors.purple,
            },
        },
    },

    plugins: [
        forms,
        typography,
        aspectratio,
    ],
};
