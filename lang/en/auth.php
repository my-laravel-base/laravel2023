<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'These credentials <strong>do not match</strong> our records.',
    'password'      => 'The provided password <strong>is incorrect</strong>.',
    'throttle'      => '<strong>Too many</strong> login attempts. Please try again in :seconds seconds.',

];
