<aside class="navigation">
    <nav class="header">
        <x-sections.aside.header />
    </nav>
    <nav class="main">
        <x-sections.aside.main />
    </nav>
    <nav class="footer">
        <x-sections.aside.footer />
    </nav>
</aside>
