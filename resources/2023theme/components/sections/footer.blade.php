<footer>
    <section class="flex flex-col items-center justify-center text-sm">
        <article class="mx-2">
            &copy; {{ date('Y') }} <strong aria-label="{{ config('app.name') }}">{{ config('app.name') }}</strong>. All Rights
            Reserved.</span>
        </article>
    </section>
    <section class="flex flex-row items-center justify-center text-sm">
        <article class="flex">
            <a href="{{ route('dashboard') }}" class="mx-2" aria-label="{{ __('Privacy') }}">{{ __('Privacy') }}</a>

            <a href="{{ route('dashboard') }}" class="mx-2" aria-label="{{ __('Cookies') }}">{{ __('Cookies') }}</a>
        </article>
    </section>
</footer>
