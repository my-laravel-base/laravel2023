@props(['title' => null])
<header>
    <section class="flex flex-row">
        @if ($title == 'Landing')
            <x-helpers.link :route="route('dashboard')" title="{{ __(config('app.name')) }}">
                <x-brand.logo class="w-12 h-12 mr-4 -mt-1.5 leading-none" />
            </x-helpers.link>
        @endif
        <h1>{!! $title ? Str::title(__($title)) : __(config('app.name')) !!}</h1>
    </section>
    <section class="flex flex-row items-center justify-center">
        @guest
            {{-- LOGOUT SYSTEM VERSION --}}
            <x-menu.item class="version" type="div" nolink>{{ __(' v:number', ['number' => config('app.version')]) }}</x-menu.item>
            {{-- LOGOUT THEME TOOGLER --}}
            <x-helpers.theme-toggler />
        @endguest

        @auth
            {{-- NOTIFICATIONS --}}
            <x-menu.dropdown class="notifications" type="rb">
                <x-slot name="opener">
                    <x-icons.bellactive />
                </x-slot>
                <x-slot name="submenu">
                    <ul>
                        <x-menu.item class="menu-header-center" nolink>
                            {{ __('Notifications') }}
                        </x-menu.item>
                    </ul>
                    <ul>
                        <x-menu.item class="info" nolink>
                            <x-icons.circleinfo />
                            <p>{!! __('This is a regular notification.') !!}</p>
                        </x-menu.item>
                        <x-menu.item class="success" title="{{ __('SUCCESS') }}" nolink>
                            <x-icons.circlecheck />
                            <p>{!! __('<strong>SUCCESS!</strong> This is a success notification.') !!}</p>
                        </x-menu.item>
                        <x-menu.item class="danger" title="{{ __('ALERT') }}" nolink>
                            <x-icons.circlex />
                            <p>{!! __('<strong>ERROR!</strong> This is an error notification.') !!}</p>
                        </x-menu.item>
                        <x-menu.item class="warning" title="{{ __('WARNING') }}" nolink>
                            <x-icons.circleerror />
                            <p>{!! __(
                                '<strong>WARNING!</strong> This is a warning notification. This is a warning notification. This is a warning notification. This is a warning notification.',
                            ) !!}</p>
                        </x-menu.item>
                    </ul>
                </x-slot>
            </x-menu.dropdown>
        @endauth
    </section>
</header>
