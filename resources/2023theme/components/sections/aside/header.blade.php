<ul>
    <x-menu.item :route="route('landing')" :title="__(config('app.name'))">
        <x-brand.logo />
    </x-menu.item>
</ul>
