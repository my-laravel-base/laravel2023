<ul>
    <x-menu.item :route="route('dashboard')" :active="request()->routeIs('dashboard')" title="{{ __('Dashboard') }}">
        <x-icons.dashboard />
    </x-menu.item>
    @UserCan('view-a-list-of-users')
        <x-menu.item :route="route('user.list')" :active="request()->routeIs('user.*') || request()->routeIs('profile.*')" title="{{ __('Users List') }}">
            <x-icons.users />
        </x-menu.item>
    @endUserCan
    <x-menu.item route="#" title="{{ __('Section') }}">
        <x-icons.section />
    </x-menu.item>
    <x-menu.item route="#" title="{{ __('Section') }}">
        <x-icons.section />
    </x-menu.item>
    <x-menu.item route="#" title="{{ __('Schedule') }}">
        <x-icons.calendar />
    </x-menu.item>
    <x-menu.item route="#" title="{{ __('Analytics') }}">
        <x-icons.analytics />
    </x-menu.item>
</ul>

<ul>
    @UserCan('view-detailed-changelog')
        <x-menu.item class="version" :route="route('changelog')">{{ __(' v:number', ['number' => config('app.version')]) }}</x-menu.item>
    @else
        <li class="@UserCan(['view-a-list-of-roles', 'view-a-list-of-permissions'])
version
@else
version pb-2
@endUserCan">
            {{ __(' v:number', ['number' => config('app.version')]) }}</li>
    @endUserCan

    @UserCan(['view-a-list-of-roles', 'view-a-list-of-permissions'])
        <x-menu.dropdown type="br" frmt="li">
            <x-slot name="opener">
                <x-icons.cog />
            </x-slot>
            <x-slot name="submenu">
                @UserCan(['view-a-list-of-roles', 'view-a-list-of-permissions'])
                    <ul>
                        <x-menu.item class="menu-header-user" nolink>{{ __('Access Level Control Settings') }}</x-menu.item>
                    </ul>
                @endUserCan
                <ul>
                    @UserCan('view-a-list-of-roles')
                        <x-menu.item :route="route('acl.roles.index')" :active="request()->routeIs('acl.roles.index')" title="{{ __('Access Control Level - Roles') }}">
                            <x-icons.usershield /> {{ __('View / edit the roles') }}
                        </x-menu.item>
                    @endUserCan
                    @UserCan('view-a-list-of-permissions')
                        <x-menu.item :route="route('acl.perm.index')" :active="request()->routeIs('acl.perm.index')" title="{{ __('Access Control Level - Permissions') }}">
                            <x-icons.shieldplus /> {{ __('View / edit the permissions') }}
                        </x-menu.item>
                    @endUserCan
                </ul>
            </x-slot>
        </x-menu.dropdown>
    @endUserCan
</ul>
