<x-menu.dropdown type="br">
    <x-slot name="opener">
        @empty(Auth::user()->profile->avatar)
            <x-icons.user class="avatar" />
        @else
            <img class="avatar" alt="{{ Auth::user()->username }}" src="{{ Auth::user()->profile->avatar }}" />
        @endempty
    </x-slot>
    <x-slot name="submenu">
        <ul>
            <x-menu.item class="menu-header-user" nolink>
                {!! __('Welcome back <strong>:user</strong>!!', ['user' => Auth::user()->profile->full_name]) !!}
            </x-menu.item>
        </ul>
        <ul>
            <x-menu.item :route="route('profile.edit', ['user' => Auth::user()->username])" :active="request()->routeIs('profile.edit')" title="{{ __('Edit your profile') }}">
                <x-icons.cog /> {{ __('Edit Your Profile') }}
            </x-menu.item>
        </ul>
        <ul>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <x-menu.item :route="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();" title="{{ __('Logout') }}">
                    <x-icons.logout /> {{ __('Logout') }}
                </x-menu.item>
            </form>
        </ul>
        <ul>
            <x-menu.item class="theme-switcher" nolink>
                <x-helpers.theme-toggler /> {{ __('Light/Dark Theme Switcher') }}
            </x-menu.item>
        </ul>
    </x-slot>
</x-menu.dropdown>
