@props([
    'frname' => null, # Label "for" value and also input "name/id" value
    'lbltxt' => null, # Label text
    'inptyp' => 'checkbox', # Input type "checkbox/radio"
    'valinf' => null, # Input value
    'chckst' => false, # Checked status
    'plctxt' => false, # Placeholder Text
    'disabled' => false, # Disable an input
    'readonly' => false, # Read only input
    'required' => false, # Required input browser alert
    'autofocus' => false, # Autofocus input on landing
    'autocomplete' => false, # Autocomplete input with submitted "for/name/id" value
    'notify' => false, # Error notification messages
])
@php
    $clsarr = [];
    $inperr = $notify ? 'error' : '';
    $inplbl = empty($lbltxt) ? 'no-label' : '';
    if ($notify) {
        $clsarr[] = 'error';
    }
    if (empty($lbltxt)) {
        $clsarr[] = 'no-label';
    }
    if ($disabled) {
        $clsarr[] = 'disabled';
    }
    if ($readonly) {
        $clsarr[] = 'readonly';
    }
@endphp
<div {{ $attributes->merge(['class' => 'checkradio-block']) }}>
    <input id="{{ $frname }}" name="{{ $frname }}" type="{{ $inptyp }}"
        class="@if ($inptyp == 'checkbox') form-checkbox @else form-radio @endif" value="{{ $valinf }}"
        @checked($chckst) />
    @if ($lbltxt)
        <label for="{{ $frname }}" class="{{ implode(' ', $clsarr) }}">
            @if ($required)
                <span class="required" title="{{ __($lbltxt . ' is required') }}">
                    <span>{{ __($lbltxt) }}</span> *
                </span>
            @else
                {{ __($lbltxt) }}
            @endif
        </label>
    @endif
    @if ($notify)
        <x-icons.circleerror class="{{ $inplbl }} {{ $inperr }}" />
    @endif
</div>
