@props([
    'frname' => null, # Label "for" value and also input "name/id" value
    'lbltxt' => null, # Label text
    'valsel' => null, # Selected value
    'valist' => [], # Options values
    'disabled' => false, # Disable an input
    'required' => false, # Required input browser alert
    'autofocus' => false, # Autofocus input on landing
    'autocomplete' => false, # Autocomplete input with submitted "for/name/id" value
    'notify' => false, # Error notification messages
])
@php
    $lblcls = $notify ? 'error' : '';
    $inperr = $notify ? 'error' : '';
    $inplbl = empty($lbltxt) ? 'no-label' : '';
    $inpdis = $disabled ? 'disabled' : '';
@endphp
<div {{ $attributes->merge(['class' => 'input-block']) }}>
    @if ($lbltxt)
        <label for="{{ $frname }}" class="{{ $lblcls }} {{ $inpdis }}">
            @if ($required)
                <span class="required" title="{{ __($lbltxt . ' is required') }}">
                    <span>{{ __($lbltxt) }}</span> *
                </span>
            @else
                {{ __($lbltxt) }}
            @endif
        </label>
    @endif
    <select id="{{ $frname }}" name="{{ $frname }}"
        class="form-select {{ $inplbl }} {{ $inperr }} {{ $inpdis }}" {{ $required ? 'required' : '' }}
        {{ $autofocus ? 'autofocus' : '' }} {{ $autocomplete ? "autocomplete=$frname" : '' }} {{ $disabled ? 'disabled' : '' }}>
        @foreach ($valist as $validx => $item)
            <option value="{{ $validx }}" @if ($valsel == $validx) selected @endif>{{ __($item) }}</option>
        @endforeach
    </select>
    @if ($notify)
        <x-icons.circleerror class="{{ $inplbl }} {{ $inperr }}" />
    @endif
</div>
