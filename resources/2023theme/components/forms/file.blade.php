@props([
    'frname' => null, # Label "for" value and also input "name/id" value
    'lbltxt' => null, # Label text
    'valinf' => null, # Input value
    'plctxt' => false, # Placeholder Text
    'disabled' => false, # Disable an input
    'required' => false, # Required input browser alert
    'autofocus' => false, # Autofocus input on landing
    'autocomplete' => false, # Autocomplete input with submitted "for/name/id" value
    'notify' => false, # Error notification messages
])
@php
    $lblcls = $notify ? 'error' : '';
    $inperr = $notify ? 'error' : '';
    $inplbl = empty($lbltxt) ? 'no-label' : '';
    $inpdis = $disabled ? 'disabled' : '';
@endphp
<div {{ $attributes->merge(['class' => 'input-block file-block']) }}>
    @if ($lbltxt)
        <label for="{{ $frname }}" class="{{ $lblcls }} {{ $inpdis }}">
            {{ __($lbltxt) }}
        </label>
    @endif
    @if ($frname == 'avatar')
        <div class="icon">
        @empty($valinf)
            <x-icons.user />
        @else
            <img alt="{{ __($lbltxt) }}" src="{{ $valinf }}" />
        @endempty
@endif
<input id="{{ $frname }}" name="{{ $frname }}" type="file"
    class="form-input {{ $inplbl }} {{ $inperr }} {{ $inpdis }}" {{ $plctxt ? 'placeholder=' . __($plctxt) : '' }}
    {{ $required ? 'required' : '' }} {{ $autofocus ? 'autofocus' : '' }} {{ $autocomplete ? "autocomplete=$frname" : '' }}
    {{ $disabled ? 'disabled' : '' }} />
@if ($notify)
    <x-icons.circleerror class="{{ $inplbl }} {{ $inperr }}" />
@endif
</div>
</div>
