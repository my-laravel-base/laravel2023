@props(['btntyp' => 'submit', 'btnico' => false])
<button type="{{ $btntyp }}" {{ $attributes->merge(['class' => 'btn flex flex-row space-x-1']) }}>
    @includeWhen($btnico, 'components.icons.' . $btnico)
    <span>{{ $slot }}</span>
</button>
