@props(['btntyp' => 'submit', 'btnico' => false])
<button type="{{ $btntyp }}"
    {{ $attributes->merge(['class' => 'btn flex flex-row space-x-1 bg-success-500 hover:bg-success-400 dark:bg-success-500 dark:hover:bg-success-600']) }}>
    @includeWhen($btnico, 'components.icons.' . $btnico)
    <span>{{ $slot }}</span>
</button>
