@props(['btntyp' => 'submit', 'btnico' => false])
<button type="{{ $btntyp }}"
    {{ $attributes->merge(['class' => 'btn flex flex-row space-x-1 bg-danger-500 hover:bg-danger-400 dark:bg-danger-400 dark:hover:bg-danger-500']) }}>
    @includeWhen($btnico, 'components.icons.' . $btnico)
    <span>{{ $slot }}</span>
</button>
