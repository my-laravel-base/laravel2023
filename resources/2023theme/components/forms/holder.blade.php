@props([
    'method' => 'POST',
    'forurl' => '',
])
<form method="{{ $method }}" action="{{ $forurl }}" {{ $attributes->merge(['class' => '']) }}>
    @csrf
    {{ $slot }}
</form>
