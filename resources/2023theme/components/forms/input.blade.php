@props([
    'frname' => null, # Label "for" value and also input "name/id" value
    'lbltxt' => null, # Label text
    'inptyp' => 'text', # Input type "text/password/email/etc"
    'valinf' => null, # Input value
    'plctxt' => false, # Placeholder Text
    'disabled' => false, # Disable an input
    'readonly' => false, # Read only input
    'required' => false, # Required input browser alert
    'autofocus' => false, # Autofocus input on landing
    'autocomplete' => false, # Autocomplete input with submitted "for/name/id" value
    'notify' => false, # Error notification messages
])
@php
    $clsarr = [];
    $inperr = $notify ? 'error' : '';
    $inplbl = empty($lbltxt) ? 'no-label' : '';
    if ($notify) {
        $clsarr[] = 'error';
    }
    if (empty($lbltxt)) {
        $clsarr[] = 'no-label';
    }
    if ($disabled) {
        $clsarr[] = 'disabled';
    }
    if ($readonly) {
        $clsarr[] = 'readonly';
    }
@endphp
<div {{ $attributes->merge(['class' => 'input-block']) }}>
    @if ($lbltxt)
        <label for="{{ $frname }}" class="{{ implode(' ', $clsarr) }}">
            @if ($required)
                <span class="required" title="{{ __($lbltxt . ' is required') }}">
                    <span>{{ __($lbltxt) }}</span> *
                </span>
            @else
                {{ __($lbltxt) }}
            @endif
        </label>
    @endif
    <input id="{{ $frname }}" name="{{ $frname }}" type="{{ $inptyp }}" value="{{ $valinf }}"
        class="form-input {{ implode(' ', $clsarr) }}"{{ $required ? 'required' : '' }} {{ $autofocus ? 'autofocus' : '' }}
        {{ $autocomplete ? "autocomplete=$frname" : '' }} {{ $disabled ? 'disabled' : '' }} {{ $readonly ? 'readonly' : '' }}
        @if ($plctxt) placeholder="{{ $plctxt }}" @endif />
    @if ($notify)
        <x-icons.circleerror class="{{ $inplbl }} {{ $inperr }}" />
    @endif
</div>
