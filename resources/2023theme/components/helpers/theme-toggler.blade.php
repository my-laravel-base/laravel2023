<div {{ $attributes->merge(['class' => 'theme-toggler']) }}>
    <button type="button" x-bind:class="darkMode ? 'darkness' : 'lightness'" x-on:click="darkMode = !darkMode" role="switch"
        aria-checked="false">
        <span class="sr-only">Theme mode toggle</span>
        <span class="holder">
            <span class="moon" aria-hidden="true"><x-icons.moon /></span>
            <span class="sun" aria-hidden="true"><x-icons.sun /></span>
        </span>
    </button>
</div>
