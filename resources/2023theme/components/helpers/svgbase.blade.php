<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="1.5" stroke="transparent" fill="none" stroke-linecap="round"
    stroke-linejoin="round" {{ $attributes->merge(['class' => '']) }}>
    {{ $slot }}
</svg>
