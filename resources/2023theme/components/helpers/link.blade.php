@props([
    'route' => '#',
    'target' => '_self',
    'title' => null,
])
<a href="{{ $route }}" target="{{ $target }}" @isset($title) title="{{ $title }}" @endisset()
    {{ $attributes->merge(['class' => '']) }}>
    {{ $slot }}
</a>
