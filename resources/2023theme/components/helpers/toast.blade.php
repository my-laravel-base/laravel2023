@props([
    'type' => null,
    'info',
    'errors',
    'success',
    'warning',
    'status',
])
@php
    if ($info || $status) {
        $type = 'info';
    } elseif ($success) {
        $type = 'success';
    } elseif ($errors->any()) {
        $type = 'errors';
    } elseif ($warning) {
        $type = 'warning';
    }
@endphp
@if ($status || $success || $info || $errors->any() || $warning)
    <div x-data="{ show: false }" x-cloak x-show="show" x-transition:enter.duration.400ms x-transition:leave.duration.400ms
        x-init="$nextTick(() => { show = !show });
        setTimeout(() => show = !show, 5000);" class="toast {{ $type }}">
        <div class="icon">
            @if ($info || $status == 'verification-link-sent' || $status)
                <x-icons.circleinfo />
            @elseif ($errors->any())
                <x-icons.circlex />
            @elseif ($success)
                <x-icons.circlecheck />
            @elseif ($warning)
                <x-icons.circleerror />
            @endif
        </div>
        <div class="group">
            @if ($info)
                <div class="text">{!! $info !!}</div>
            @elseif ($status == 'verification-link-sent')
                <div class="text">
                    {!! __(
                        'A new <strong>verification link</strong> has been sent to the <strong>email address</strong> you provided during registration.',
                    ) !!}
                </div>
            @elseif ($status)
                <div class="text">{!! $status !!}</div>
            @elseif ($errors->any())
                @foreach ($errors->all() as $message)
                    <div class="text">{!! $message !!}</div>
                @endforeach
            @elseif ($success)
                <div class="text">{!! $success !!}</div>
            @elseif ($warning)
                <div class="text">{!! $warning !!}</div>
            @endif
        </div>
    </div>
    </div>
@endif
