@props([
    'selection' => fake()->numberBetween(0, 4),
    'imgdata' => [
        0 => [
            'img_url' => 'images/access/01.jpg',
            'author_url' => 'https://unsplash.com/@mahdibafande?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'author' => 'Mahdi Bafande',
            'source_url' => 'https://unsplash.com/photos/a-blurry-photo-of-a-street-at-night-0WL6fOpHCxc?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'source' => 'Unsplash',
        ],
        1 => [
            'img_url' => 'images/access/02.jpg',
            'author_url' => 'https://unsplash.com/@mrthetrain?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'author' => 'Joshua Hoehne',
            'source_url' => 'https://unsplash.com/photos/tsHxW4DHzzM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'source' => 'Unsplash',
        ],
        2 => [
            'img_url' => 'images/access/03.jpg',
            'author_url' => 'https://unsplash.com/@mahdibafande?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'author' => 'Mahdi Bafande',
            'source_url' => 'https://unsplash.com/photos/SHmIM3snybs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'source' => 'Unsplash',
        ],
        3 => [
            'img_url' => 'images/access/04.jpg',
            'author_url' => 'https://unsplash.com/@wolfgang_hasselmann?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'author' => 'Wolfgang Hasselmann',
            'source_url' => 'https://unsplash.com/photos/zcZrCUKAsW0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'source' => 'Unsplash',
        ],
        4 => [
            'img_url' => 'images/access/05.jpg',
            'author_url' => 'https://unsplash.com/@omidarmin?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'author' => 'Omid Armin',
            'source_url' => 'https://unsplash.com/photos/C9g57nvO1As?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText',
            'source' => 'Unsplash',
        ],
    ],
])

<span class="absolute z-auto px-1 py-0.5 text-xs rounded-md bg-gray-400 opacity-70 bottom-3 right-3">{!! __(
    'Photo by <a class="hover:text-gray-700" href=":author_url" target="_blank"><strong>:author</strong></a> on <a class="hover:text-gray-700" href=":source_url" target="_blank"><strong>:source</strong></a>',
    [
        'author_url' => $imgdata[$selection]['author_url'],
        'author' => $imgdata[$selection]['author'],
        'source_url' => $imgdata[$selection]['source_url'],
        'source' => $imgdata[$selection]['source'],
    ],
) !!}</span>
<img class="object-cover w-full h-screen" src="{{ asset($imgdata[$selection]['img_url']) }}" />
