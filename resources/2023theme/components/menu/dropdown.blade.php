@props([
    'type' => 'cd',
    'frmt' => 'div',
])
{{--
    Type of dropdowns:
    cd -> center down
    ct -> center top
    ld -> left down
    lt -> left top
    rd -> right down
    rt -> right top
--}}
@if ($frmt == 'li')
    <li {{ $attributes->merge(['class' => 'dropdown dir-' . $type]) }} x-data="{ open: false, toggle() { if (this.open) { return this.close() } this.open = true }, close(focusAfter) { this.open = false; } }"
        x-on:keydown.escape.prevent.stop="close($refs.button)" x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
        x-id="['dropdown-button']">
    @else
        <div {{ $attributes->merge(['class' => 'dropdown dir-' . $type]) }} x-data="{ open: false, toggle() { if (this.open) { return this.close() } this.open = true }, close(focusAfter) { this.open = false; } }"
            x-on:keydown.escape.prevent.stop="close($refs.button)" x-on:focusin.window="! $refs.panel.contains($event.target) && close()"
            x-id="['dropdown-button']">
@endif

<!-- Dropdown Button-->
<button x-ref="button" x-on:click="toggle()" :aria-expanded="open" :aria-controls="$id('dropdown-button')" type="button">
    {{ $opener }}
</button>
<!-- Dropdown Panel-->
<div x-ref="panel" x-show="open" x-transition.origin.top.left x-on:click.outside="close($refs.button)" :id="$id('dropdown-button')"
    style="display: none" class="dropdown-submenu">
    {{ $submenu }}
</div>

@if ($frmt == 'div')
    </div>
@else
    </li>
@endif
