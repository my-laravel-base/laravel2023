@props([
    'route' => '#',
    'active' => false,
    'target' => '_self',
    'title' => null,
    'type' => 'li',
    'nolink',
])
@php
    $classes = $active ? 'current-item ' : '';
@endphp
@switch($type)
    @case('li')
        @if (isset($nolink))
            <li {{ $attributes->merge(['class' => '']) }}>{{ $slot }}</li>
        @else
            <li {{ $attributes->merge(['class' => $classes]) }}>
                <x-helpers.link route="{{ $route }}" target="{{ $target }}" title="{{ $title }}">
                    {{ $slot }}
                </x-helpers.link>
            </li>
        @endisset
    @break

    @case('div')
        @if (isset($nolink))
            <div {{ $attributes->merge(['class' => '']) }}>{{ $slot }}</div>
        @else
            <div {{ $attributes->merge(['class' => $classes]) }}>
                <x-helpers.link route="{{ $route }}" target="{{ $target }}" title="{{ $title }}">
                    {{ $slot }}
                </x-helpers.link>
            </div>
        @endisset
    @break
@endswitch
