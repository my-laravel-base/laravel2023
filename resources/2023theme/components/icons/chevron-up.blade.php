@props(['class' => 'w-5 h-5 stroke-current fill-none'])
<x-helpers.svgbase class="{{ $class }}">
    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
    <path d="M6 15l6 -6l6 6" />
</x-helpers.svgbase>
