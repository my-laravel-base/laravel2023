@props(['class' => 'w-5 h-5 stroke-current fill-none'])
<x-helpers.svgbase class="{{ $class }}">
    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
    <path d="M18 6l-12 12" />
    <path d="M6 6l12 12" />
</x-helpers.svgbase>
