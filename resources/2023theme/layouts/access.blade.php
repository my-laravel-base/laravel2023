<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{{ $title ? $title . ' |' : '' }} {{ config('app.name', 'Laravel') }}</title>

    <meta name="application-name" content="{{ config('app.name') }}" />
    <meta name="author" content="{{ config('app.author') }}" />
    <meta name="description" content="{{ config('app.description') }}" />
    <meta name="keywords" content="{{ config('app.keywords') }}" />

    <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large" />
    <meta name="generator" content="{{ __('Visual Studio Code') }}" />
    <meta name="creator" content="{{ config('app.author') }}" />
    <meta name="publisher" content="{{ config('app.author') }}" />
    <meta name="color-scheme" content="light dark" />

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#6f32be">
    <meta name="msapplication-TileColor" content="#dabeff">
    <meta name="theme-color" content="#dabeff">

    <meta property="og:url" content="{{ config('app.url') }}" />
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:title" content="{{ config('app.title') }}" />
    <meta property="og:description" content="{{ config('app.description') }}" />
    <meta property="og:image" content="{{ asset('images/logo.svg') }}" />
    <meta property="og:image:type" content="image/svg" />
    <meta property="og:image:width" content="512" />
    <meta property="og:image:height" content="512" />

    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:title" content="{{ config('app.title') }}" />
    <meta property="twitter:description" content="{{ config('app.description') }}" />
    <meta property="twitter:image:src" content="{{ asset('images/logo.svg') }}" />
    <meta property="twitter:image:width" content="512" />
    <meta property="twitter:image:height" content="512" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net" />

    <!-- Scripts -->
    @vite(['resources/css/theme.css', 'resources/js/theme.js'])
</head>

<body class="guest" @include('layouts.darkscript')>

    <x-helpers.noscript />

    <x-helpers.toast :status="session('status')" :success="session('success')" :info="session('info')" :errors="$errors" :warning="session('warning')" />

    <section class="content">

        <main>{{ $slot }}</main>

    </section>

</body>

</html>
