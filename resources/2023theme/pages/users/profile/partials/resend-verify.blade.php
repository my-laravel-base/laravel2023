<h2 class="mb-4">{{ __('E-mail verification') }}</h2>

<x-forms.holder forurl="{{ route('verification.send') }}">
    <p class="mb-4">
        {{ __('The email address has not been verified yet. Click the button to re-send the verification email.') }}
    </p>
    <x-forms.button class="w-full">{{ __('Re-send the verification email') }}</x-forms.button>
</x-forms.holder>
