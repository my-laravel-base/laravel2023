<h2 class="mb-4">{{ __('Update Password') }}</h2>

<x-forms.holder forurl="{{ route('password.update') }}">
    @method('put')

    <p class="mb-4">
        {{ __('Ensure your account is using a long, random password to stay secure.') }}
    </p>

    <x-forms.input :lbltxt="__('Current Password')" frname="current_password" inptyp="password" :valinf="old('current_password')" :notify="$errors->updatePassword->get('current_password')" required autocomplete />

    <div class="flex space-x-4">
        <x-forms.input class="w-1/2 mb-4" :lbltxt="__('New Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->updatePassword->get('password')" required
            autocomplete />

        <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Confirm New Password')" frname="password_confirmation" inptyp="password" :valinf="old('password_confirmation')"
            :notify="$errors->updatePassword->get('password_confirmation')" required autocomplete />
    </div>

    <div class="action-button">
        <x-forms.button btnico="circlekey">{{ __('Update') }}</x-forms.button>
    </div>

</x-forms.holder>
