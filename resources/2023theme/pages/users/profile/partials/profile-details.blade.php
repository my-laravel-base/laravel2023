<h2 class="mb-4">{{ __('Profile Information') }}</h2>

<x-forms.holder forurl="{{ route('profile.update', ['user' => $user->username]) }}" enctype="multipart/form-data">
    @method('patch')

    <p class="mb-4">
        {{ __("Update your account's profile information and email address.") }}
    </p>

    <div class="flex space-x-4">
        @UserCan('change-a-users-role')
            <x-forms.input class="w-2/4 mb-4" :lbltxt="__('Username')" frname="username" :valinf="old('username', $user->username)" :notify="$errors->get('username')" disabled />

            <x-forms.select class="w-2/4 mb-4" :lbltxt="__('Role type')" frname="role_id" :valsel="old('role_id', $user->role->slug)" :notify="$errors->get('role_id')" :valist="$user->role->list_all_in_array()" required
                autocomplete />
        @else
            <x-forms.input class="w-full mb-4" :lbltxt="__('Username')" frname="username" :valinf="old('username', $user->username)" :notify="$errors->get('username')" disabled />
        @endUserCan
    </div>

    <x-forms.input class="mb-4" :lbltxt="__('E-mail')" frname="email" :valinf="old('email', $user->email)" :notify="$errors->get('email')" required autofocus autocomplete />

    <div class="flex space-x-4">
        <x-forms.input class="w-1/3 mb-4" :lbltxt="__('First Name')" frname="first_name" :valinf="old('first_name', $user->profile->first_name)" :notify="$errors->get('first_name')" required
            autocomplete />

        <x-forms.input class="w-1/3 mb-4" :lbltxt="__('Middle Name')" frname="middle_name" :valinf="old('middle_name', $user->profile->middle_name)" :notify="$errors->get('middle_name')" autocomplete />

        <x-forms.input class="w-1/3 mb-4" :lbltxt="__('Last Name')" frname="last_name" :valinf="old('last_name', $user->profile->last_name)" :notify="$errors->get('last_name')" required
            autocomplete />
    </div>

    <div class="flex space-x-4">
        <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Date of Birth')" frname="date_of_birth" :plctxt="__('Mmm, dd YYYY')" :valinf="old('date_of_birth', $user->profile->date_of_birth)" :notify="$errors->get('date_of_birth')"
            required autocomplete />

        <x-forms.select class="w-1/2 mb-4" :lbltxt="__('Gender')" frname="gender" :valsel="old('gender', $user->profile->gender)" :notify="$errors->get('gender')" :valist="config('options.gender')"
            required autocomplete />
    </div>

    <div class="flex space-x-4">
        <x-forms.select class="w-1/2 mb-4" :lbltxt="__('Language')" frname="language" :valsel="old('language', $user->profile->language)" :notify="$errors->get('language')" :valist="config('options.language')"
            autocomplete />

        <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Social Security')" frname="social_security" :plctxt="__('00-AAA-000000')" :valinf="old('social_security', $user->profile->social_security)"
            :notify="$errors->get('social_security')" autocomplete />
    </div>

    <div class="flex space-x-4">
        <x-forms.file class="w-full mb-4" :lbltxt="__('Avatar')" frname="avatar" :valinf="old('avatar', $user->profile->avatar)" :notify="$errors->get('avatar')" />
    </div>

    <div class="action-button">
        <x-forms.button btnico="save">{{ __('Save') }}</x-forms.button>
    </div>
</x-forms.holder>
