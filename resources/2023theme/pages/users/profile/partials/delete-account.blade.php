<h2 class="mb-4">{{ __('Delete Account') }}</h2>

<p class="mb-4 leading-5">
    {{ __('Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.') }}
</p>

<x-forms.btndanger btnico="circlex" x-data=""
    x-on:click.prevent="$dispatch('open-modal', 'confirm-user-deletion')">{{ __('Delete Account') }}</x-forms.btndanger>

<!-- Modal -->
<x-helpers.modal name="confirm-user-deletion" :show="$errors->userDeletion->isNotEmpty()" focusable>
    <x-forms.holder forurl="{{ route('user.delete', ['user' => $user->username]) }}" class="p-4">
        @method('delete')

        <h3 class="mb-4">{{ __('Are you sure you want to delete your account?') }}</h3>

        <p class="mb-4 leading-5">
            {{ __('Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.') }}
        </p>

        <x-forms.input :lbltxt="__('Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->userDeletion->get('password')" required autocomplete />

        <div class="action-button">
            <x-forms.btndanger btnico="circlex" x-on:click="$dispatch('close')">{{ __('Cancel') }}</x-forms.btndanger>

            <x-forms.btnsuccess btnico="circlecheck">{{ __('Delete Account') }}</x-forms.btnsuccess>
        </div>
    </x-forms.holder>
</x-helpers.modal>
