<x-application-layout :title="__(':user Profile', ['user' => $pgtl])">
    <section class="profile new">
        <x-forms.holder forurl="{{ route('profile.store') }}" enctype="multipart/form-data">
            @method('POST')

            <article class="details">
                <h2 class="mb-4">{{ __('Profile Information') }}</h2>

                <p class="mb-4">
                    {{ __('Add the profile information and email address.') }}
                </p>

                <div class="flex space-x-4">
                    <x-forms.input class="w-2/3 mb-4" :lbltxt="__('Username')" frname="username" :valinf="old('username')" :notify="$errors->get('username')" required
                        autocomplete />

                    @if (Auth::user()->HasPermissionTo('change-a-users-role'))
                        <x-forms.select class="w-1/2 mb-4" :lbltxt="__('Role type')" frname="role_id" :valsel="old('role_id')" :notify="$errors->get('role_id')"
                            :valist="Auth::user()->role->list_all_in_array()" required autocomplete />
                    @else
                        <p class="flex items-center justify-center w-1/3 mb-4 text-primary-300">
                            {!! __('Role type: <em>:role</em>', ['role' => config('defaults.basic.role')]) !!}
                        </p>
                    @endif
                </div>

                <x-forms.input class="mb-4" :lbltxt="__('E-mail')" frname="email" :valinf="old('email')" :notify="$errors->get('email')" required autofocus
                    autocomplete />

                <div class="flex space-x-4">
                    <x-forms.input class="w-1/3 mb-4" :lbltxt="__('First Name')" frname="first_name" :valinf="old('first_name')" :notify="$errors->get('first_name')" required
                        autocomplete />

                    <x-forms.input class="w-1/3 mb-4" :lbltxt="__('Middle Name')" frname="middle_name" :valinf="old('middle_name')" :notify="$errors->get('middle_name')"
                        autocomplete />

                    <x-forms.input class="w-1/3 mb-4" :lbltxt="__('Last Name')" frname="last_name" :valinf="old('last_name')" :notify="$errors->get('last_name')" required
                        autocomplete />
                </div>

                <div class="flex space-x-4">
                    <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Date of Birth')" frname="date_of_birth" :plctxt="__('Mmm, dd YYYY')" :valinf="old('date_of_birth')"
                        :notify="$errors->get('date_of_birth')" required autocomplete />

                    <x-forms.select class="w-1/2 mb-4" :lbltxt="__('Gender')" frname="gender" :valsel="old('gender')" :notify="$errors->get('gender')"
                        :valist="config('options.gender')" required autocomplete />
                </div>

                <div class="flex space-x-4">
                    <x-forms.select class="w-1/2 mb-4" :lbltxt="__('Language')" frname="language" :valsel="old('language')" :notify="$errors->get('language')"
                        :valist="config('options.language')" autocomplete />

                    <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Social Security')" frname="social_security" :plctxt="__('00-AAA-000000')" :valinf="old('social_security')"
                        :notify="$errors->get('social_security')" autocomplete />
                </div>

                <div class="flex space-x-4">
                    <x-forms.file class="w-full mb-4" :lbltxt="__('Avatar')" frname="avatar" :valinf="old('avatar')" :notify="$errors->get('avatar')" />
                </div>

                <p class="mt-16 mb-4">
                    {{ __('Ensure the new user has a long, random password to stay secure.') }}
                </p>

                <div class="flex space-x-4">
                    <x-forms.input class="w-1/2 mb-4" :lbltxt="__('New Password')" frname="password" inptyp="password" :valinf="old('password')"
                        :notify="$errors->get('password')" required autocomplete />

                    <x-forms.input class="w-1/2 mb-4" :lbltxt="__('Confirm New Password')" frname="password_confirmation" inptyp="password" :valinf="old('password_confirmation')"
                        :notify="$errors->get('password_confirmation')" required autocomplete />
                </div>

                <div class="action-button">
                    <x-forms.button btnico="userplus">{{ __('Create') }}</x-forms.button>
                </div>
            </article>

            <article class="details"></article>

            <article class="details"></article>

        </x-forms.holder>
    </section>
</x-application-layout>
