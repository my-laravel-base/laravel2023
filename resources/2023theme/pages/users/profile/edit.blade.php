<x-application-layout :title="__(':user Profile', ['user' => $pgtl])">
    <section class="profile">
        <article class="details">
            @include('pages.users.profile.partials.profile-details')
        </article>

        @if ($user->id == Auth::user()->id)
            <article class="details">
                @include('pages.users.profile.partials.update-password')
            </article>

            <article class="delete">
                @include('pages.users.profile.partials.delete-account')
            </article>
        @endif

        @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && !$user->hasVerifiedEmail())
            <article class="details half">
                @include('pages.users.profile.partials.resend-verify')
            </article>
        @endif
    </section>
</x-application-layout>
