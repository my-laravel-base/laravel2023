<x-application-layout :title="__('User list')">
    <section class="user">
        <p>{{ __('This is a list with all the available users in the system.') }}</p>
        <br />
        <div class="detailed-list">
            <div class="detailed-header">
                <div class="w-1/12">&nbsp;</div>
                @UserCan('change-a-users-role')
                    <div class="w-1/12">{{ __('Username') }}</div>
                    <div class="w-2/12">{{ __('Role Type') }}</div>
                    <div class="w-2/12">{{ __('Name') }}</div>
                    <div class="w-2/12">{{ __('E-mail') }}</div>
                @else
                    <div class="w-2/12">{{ __('Username') }}</div>
                    <div class="w-2/12">{{ __('Name') }}</div>
                    <div class="w-3/12">{{ __('E-mail') }}</div>
                @endUserCan
                <div class="w-1/12">{{ __('Age') }}</div>
                <div class="w-2/12">{{ __('Registered on') }}</div>
                <div class="w-1/12 actions">
                    @UserCan('create-a-user')
                        <x-helpers.link :route="route('profile.create')" class="mini-btn edit">
                            <x-icons.userplus />
                        </x-helpers.link>
                    @endUserCan
                </div>
            </div>
            @foreach ($users as $user)
                <div class="detailed-row @if ($loop->odd) odd @else even @endif">
                    <div class="flex items-center justify-center w-1/12">
                        @if (empty($user->profile->avatar))
                            <x-icons.user class="avatar mini" />
                        @else
                            <img class="avatar mini" alt="{{ $user->username }}" src="{{ $user->profile->avatar }}" />
                        @endif
                    </div>
                    @UserCan('change-a-users-role')
                        <div class="w-1/12">{{ $user->username }}</div>
                        <div class="w-2/12 text-primary-500">{{ $user->role->name }}</div>
                        <div class="w-2/12 text-left">{{ $user->profile->full_name }}</div>
                        <div class="w-2/12 text-left">{{ $user->email }}</div>
                    @else
                        <div class="w-2/12">{{ $user->username }}</div>
                        <div class="w-2/12 text-left">{{ $user->profile->full_name }}</div>
                        <div class="w-3/12 text-left">{{ $user->email }}</div>
                    @endUserCan
                    <div class="w-1/12">{{ $user->profile->user_age . __(' yrs.') }}</div>
                    <div class="w-2/12">{{ $user->created_at }}</div>
                    <div class="w-1/12 actions">
                        @UserCan(['update-a-user', 'update-other-users'])
                            <x-helpers.link :route="route('profile.edit', ['user' => $user->username])" class="mini-btn edit">
                                <x-icons.useredit />
                            </x-helpers.link>
                        @endUserCan
                        @UserCan('delete-a-user')
                            <x-forms.holder forurl="{{ route('user.delete', ['user' => $user->username]) }}">
                                @method('DELETE')
                                <x-forms.btndanger type="submit" btnico="trash" class="small" />
                            </x-forms.holder>
                        @endUserCan
                    </div>
                </div>
            @endforeach

            @if ($users->hasPages())
                <div class="detailed-footer">
                    <div class="w-full">{{ $users->links() }}</div>
                </div>
            @endif
        </div>
    </section>
</x-application-layout>
