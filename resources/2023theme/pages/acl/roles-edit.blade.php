<x-application-layout :title="__('Details for role: :role', ['role' => $role->name])">
    <section class="access-control">
        <x-forms.holder forurl="{{ route('acl.roles.update', ['role' => $role->slug]) }}">
            @method('patch')
            <div class="flex w-full space-x-4">
                <div class="flex flex-col w-1/6">
                    <x-forms.input class="mb-4" :lbltxt="__('Role name')" frname="name" :valinf="old('name', $role->name)" :notify="$errors->get('name')" required
                        autocomplete />

                    <x-forms.input class="mb-4" :lbltxt="__('Slug')" frname="slug" :valinf="old('slug', $role->slug)" :notify="$errors->get('slug')" required readonly
                        autocomplete />
                </div>
                <div class="grid w-5/6 grid-cols-5 gap-4">
                    @foreach ($permissions as $permission)
                        <x-forms.checkradio :lbltxt="__($permission->name)" frname="permissions[]" :valinf="old('rule', $permission->id)" :chckst="$role->IsAllowed($permission->rule)"
                            class="col-span-1" />
                    @endforeach
                </div>
            </div>

            <div class="flex items-center justify-between w-full pt-4 mt-4 space-x-4 border-t border-primary-300">
                <x-helpers.link :route="route('acl.roles.index')">
                    <x-icons.chevron-left class="mr-1" /> {{ __('Go back') }}
                </x-helpers.link>
                @UserCan('update-a-role')
                    <x-forms.btnsuccess type="submit" btnico="circlecheck">{{ __('Update') }}</x-forms.btnsuccess>
                @endUserCan
            </div>
        </x-forms.holder>
    </section>
</x-application-layout>
