<x-application-layout :title="__('List of permissions for the access roles')">
    <section class="access-control">

        @UserCan('create-a-permission')
            {{-- The form below creates new permissions --}}
            <x-forms.holder forurl="{{ route('acl.perm.store') }}">
                <div class="flex items-center justify-center w-full mb-4">
                    <div class="w-3/12">
                        <x-forms.input class="m-0" :plctxt="__('Insert a name to create a new permission')" frname="name" :valinf="old('name')" :notify="$errors->get('name')" required
                            autocomplete />
                    </div>
                    <div class="w-2/12 px-4">
                        <x-forms.btnsuccess type="submit" btnico="shieldplus">{{ __('Create a new permission') }}</x-forms.btnsuccess>
                    </div>
                    <div class="w-7/12">&nbsp;</div>
                </div>
            </x-forms.holder>
            {{-- The form above creates new permissions --}}
        @endUserCan

        <div class="detailed-list">
            <div class="detailed-header">
                <div class="w-2/12">{{ __('Permission') }}</div>
                <div class="w-2/12">{{ __('Created on') }}</div>
                <div class="w-7/12">{{ __('Roles using it') }}</div>
                <div class="w-1/12">&nbsp;</div>
            </div>

            @UserCan('view-a-list-of-permissions')
                @foreach ($permissions as $permission)
                    <div class="detailed-row @if ($loop->odd) odd @else even @endif">
                        <div class="w-2/12 text-left">{{ $permission->name }}</div>
                        <div class="w-2/12">{{ $permission->created_at }}</div>
                        <div class="w-7/12 text-left">
                            @foreach ($permission->roles as $role)
                                {{ $role->name }}
                                @if (!$loop->last)
                                    ,
                                @endif
                            @endforeach
                        </div>
                        <div class="w-1/12 actions">
                            @UserCan('update-a-permission')
                                <x-helpers.link :route="route('acl.perm.edit', ['permission' => $permission->rule])" class="mini-btn edit">
                                    <x-icons.edit />
                                </x-helpers.link>
                            @endUserCan
                            @UserCan('delete-a-permission')
                                <x-forms.holder forurl="{{ route('acl.perm.destroy', ['permission' => $permission->rule]) }}">
                                    @method('DELETE')
                                    <x-forms.btndanger type="submit" btnico="trash" class="small" />
                                </x-forms.holder>
                            @endUserCan
                        </div>
                    </div>
                @endforeach
            @endUserCan

            @if ($permissions->hasPages())
                <div class="detailed-footer">
                    <div class="w-full">{{ $permissions->links() }}</div>
                </div>
            @endif
        </div>
    </section>
</x-application-layout>
