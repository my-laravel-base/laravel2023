<x-application-layout :title="__('Details for permission: :permission', ['permission' => $permission->name])">
    <section class="access-control">
        <x-forms.holder forurl="{{ route('acl.perm.update', ['permission' => $permission->rule]) }}">
            @method('patch')
            <div class="flex w-full space-x-4">
                <x-forms.input class="w-1/6" :lbltxt="__('Permission name')" frname="name" :valinf="old('name', $permission->name)" :notify="$errors->get('name')" required autocomplete />

                <x-forms.input class="w-1/6" :lbltxt="__('Rule')" frname="rule" :valinf="old('rule', $permission->rule)" :notify="$errors->get('rule')" required readonly
                    autocomplete />
            </div>

            <div class="flex items-center justify-between w-full pt-4 space-x-4 border-t border-primary-300">
                <x-helpers.link :route="route('acl.perm.index')">
                    <x-icons.chevron-left class="mr-1" /> {{ __('Go back') }}
                </x-helpers.link>
                @UserCan('update-a-permission')
                    <x-forms.btnsuccess type="submit" btnico="circlecheck">{{ __('Update') }}</x-forms.btnsuccess>
                @endUserCan
            </div>
        </x-forms.holder>
    </section>
</x-application-layout>
