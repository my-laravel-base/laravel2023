<x-application-layout :title="__('Access roles list')">
    <section class="access-control">

        @UserCan('create-a-role')
            {{-- The below form creates new roles --}}
            <x-forms.holder forurl="{{ route('acl.roles.store') }}">
                <div class="flex items-center justify-center w-full mb-4">
                    <div class="w-3/12">
                        <x-forms.input class="m-0" :plctxt="__('Insert a name to create a new role')" frname="name" :valinf="old('name')" :notify="$errors->get('name')" required
                            autocomplete />
                    </div>
                    <div class="w-2/12 px-4">
                        <x-forms.btnsuccess type="submit" btnico="usershield">{{ __('Create a new role') }}</x-forms.btnsuccess>
                    </div>
                    <div class="w-7/12">&nbsp;</div>
                </div>
            </x-forms.holder>
            {{-- The above form creates new roles --}}
        @endUserCan

        <div class="detailed-list">
            <div class="detailed-header">
                <div class="w-2/12">{{ __('Role') }}</div>
                <div class="w-2/12">{{ __('Last update') }}</div>
                <div class="w-7/12">{{ __('Permissions') }}</div>
                <div class="w-1/12">&nbsp;</div>
            </div>

            @UserCan('view-a-list-of-roles')
                @foreach ($roles as $role)
                    <div class="detailed-row @if ($loop->odd) odd @else even @endif">
                        <div class="w-2/12 text-left">{{ $role->name }}</div>
                        <div class="w-2/12">{{ $role->updated_at }}</div>
                        <div class="w-7/12 text-left">
                            @foreach ($role->permissions as $permission)
                                {{ $permission->name }}
                                @if (!$loop->last)
                                    ,
                                @endif
                            @endforeach
                        </div>
                        <div class="w-1/12 actions">
                            @UserCan('update-a-role')
                                <x-helpers.link :route="route('acl.roles.edit', ['role' => $role->slug])" class="mini-btn edit">
                                    <x-icons.edit />
                                </x-helpers.link>
                            @endUserCan
                            @UserCan('delete-a-role')
                                <x-forms.holder forurl="{{ route('acl.roles.destroy', ['role' => $role->slug]) }}">
                                    @method('DELETE')
                                    <x-forms.btndanger type="submit" btnico="trash" class="small" />
                                </x-forms.holder>
                            @endUserCan
                        </div>
                    </div>
                @endforeach
            @endUserCan

            @if ($roles->hasPages())
                <div class="detailed-footer">
                    <div class="w-full">{{ $roles->links() }}</div>
                </div>
            @endif
        </div>
    </section>
</x-application-layout>
