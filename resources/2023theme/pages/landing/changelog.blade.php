<x-application-layout :title="__('Changelog')">
    <section class="changelog">
        @foreach ($changelog as $item)
            <article class="detail">
                <div class="tag">
                    <x-helpers.link :route="$item->commit->web_url" target="_blank">{{ $item->tag->name }}</x-helpers.link>
                </div>
                <div class="date">{{ $item->commit->committed_date }}</div>
                <div class="title">
                    <div>{{ $item->commit->title }}</div>
                    <div>
                        <span class="dels" title="{{ __('Commit Deletions') }}">
                            {{ '-' . $item->commit->deletions }}
                        </span> <span class="adds" title="{{ __('Commit Additions') }}">
                            {{ '+' . $item->commit->additions }}
                        </span>
                    </div>
                </div>
                <div class="author">
                    <img class="avatar mini" alt="{{ $item->commit->author_name }}" src="{{ $item->commit->author_avatar }}" />
                    <x-helpers.link route="{{ 'mailto: ' . $item->commit->author_email }}" target="_blank">
                        {{ $item->commit->author_name }}
                    </x-helpers.link>
            </article>
        @endforeach
    </section>
</x-application-layout>
