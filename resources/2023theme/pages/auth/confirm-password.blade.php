<x-access-layout :title="__('Confirm Password')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <x-forms.holder forurl="{{ route('password.confirm') }}">

                <p class="mb-3 text-sm leading-5">{!! __(
                    'This is a <strong>secure area</strong> of the application. Please <strong>confirm :user\'s password</strong> before continuing.',
                    ['user' => Auth::user()->username],
                ) !!}</p>

                <x-forms.input :lbltxt="__('Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->get('password')" required autofocus
                    autocomplete />

                <x-forms.button>{{ __('Confirm') }}</x-forms.button>

            </x-forms.holder>

            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
