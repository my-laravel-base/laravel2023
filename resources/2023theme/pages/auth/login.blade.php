<x-access-layout :title="__('Login')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <x-forms.holder forurl="{{ route('login') }}">

                <x-forms.input :lbltxt="__('Username')" frname="username" :valinf="old('username')" :notify="$errors->get('username')" required autofocus autocomplete />

                <x-forms.input :lbltxt="__('Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->get('password')" required />

                <x-forms.button btnico="login">{{ __('Login') }}</x-forms.button>

                <div class="form-links">
                    @if (Route::has('register'))
                        <x-helpers.link :route="route('register')">{{ __('Create an account') }}</x-helpers.link>
                    @endif
                    @if (Route::has('password.request'))
                        <x-helpers.link route="{{ route('password.request') }}">{{ __('Recover your password') }}</x-helpers.link>
                    @endif
                </div>

            </x-forms.holder>

            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
