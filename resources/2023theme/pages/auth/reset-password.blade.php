<x-access-layout :title="__('Reset Password')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <x-forms.holder forurl="{{ route('password.store') }}">

                <!-- Password Reset Token -->
                <x-forms.input frname="token" inptyp="hidden" :valinf="$request->route('token')" />

                <x-forms.input :lbltxt="__('E-mail')" frname="email" :valinf="old('email', $request->email)" :notify="$errors->get('email')" required autofocus autocomplete />

                <x-forms.input :lbltxt="__('New Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->get('password')" required
                    autocomplete />

                <x-forms.input :lbltxt="__('Confirm New Password')" frname="password_confirmation" inptyp="password" :valinf="old('password_confirmation')" :notify="$errors->get('password_confirmation')"
                    required autocomplete />

                <x-forms.button btnico="circlekey">{{ __('Reset Password') }}</x-forms.button>

                <div class="form-links">
                    @if (Route::has('login'))
                        <x-helpers.link :route="route('login')">{{ __('Already registered? Log in') }}</x-helpers.link>
                    @endif
                    @if (Route::has('register'))
                        <x-helpers.link :route="route('register')">{{ __('Create an account') }}</x-helpers.link>
                    @endif
                </div>

            </x-forms.holder>

            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
