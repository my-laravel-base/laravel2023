<x-access-layout :title="__('E-mail verification')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <div class="flex flex-col items-center justify-center space-y-4">
                <x-forms.holder forurl="{{ route('verification.send') }}">
                    <p class="mb-3 text-sm leading-5">
                        {!! __(
                            'Thanks for signing up! Before getting started, could you <strong>verify your email address</strong> by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another by using the button below.',
                        ) !!}
                    </p>

                    <x-forms.button btnico="sentmail">{{ __('Resend Verification Email') }}</x-forms.button>
                </x-forms.holder>

                <x-forms.holder forurl="{{ route('logout') }}">
                    <p class="mb-3 text-sm leading-5">
                        {{ __('Alternatively, you can just ...') }}
                    </p>
                    <x-forms.button btnico="logout">{{ __('Log Out') }}</x-forms.button>
                </x-forms.holder>
            </div>


            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
