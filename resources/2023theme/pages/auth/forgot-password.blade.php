<x-access-layout :title="__('Login')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <x-forms.holder forurl="{{ route('password.email') }}">

                <p class="mb-3 text-sm leading-5">{!! __(
                    '<em>Forgot your password?</em> No problem. Just let us know <strong>your email address</strong> and we will email you a <strong>password reset link</strong> that will allow you to choose a new one.',
                ) !!}</p>

                <x-forms.input :lbltxt="__('E-mail')" frname="email" :valinf="old('email')" :notify="$errors->get('email')" required autofocus autocomplete />

                <x-forms.button btnico="sentmail">{{ __('Email Password Reset Link') }}</x-forms.button>

                <div class="form-links">
                    @if (Route::has('login'))
                        <x-helpers.link :route="route('login')">{{ __('Already registered? Log in') }}</x-helpers.link>
                    @endif
                    @if (Route::has('register'))
                        <x-helpers.link :route="route('register')">{{ __('Create an account') }}</x-helpers.link>
                    @endif
                </div>

            </x-forms.holder>

            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
