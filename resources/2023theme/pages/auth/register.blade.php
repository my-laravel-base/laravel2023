<x-access-layout :title="__('Register')">
    <div class="login">
        <section>
            <div class="header">
                <x-helpers.link :route="route('landing')" :title="__(config('app.name'))">
                    <x-brand.logo showname />
                </x-helpers.link>

                <span class="flex flex-row items-center justify-center">
                    {{-- LOGOUT SYSTEM VERSION --}}
                    <x-menu.item class="version" type="div" nolink>
                        {{ __(' v:number', ['number' => config('app.version')]) }}
                    </x-menu.item>
                    {{-- LOGOUT THEME TOOGLER --}}
                    <x-helpers.theme-toggler />
                </span>
            </div>

            <x-forms.holder forurl="{{ route('register') }}">

                <x-forms.input :lbltxt="__('Username')" frname="username" :valinf="old('username')" :notify="$errors->get('username')" required autofocus autocomplete />

                <div class="flex flex-row space-x-4">
                    <x-forms.input :lbltxt="__('First Name')" frname="first_name" :valinf="old('first_name')" :notify="$errors->get('first_name')" required autocomplete />

                    <x-forms.input :lbltxt="__('Middle Name')" frname="middle_name" :valinf="old('middle_name')" :notify="$errors->get('middle_name')" autocomplete />
                </div>

                <x-forms.input :lbltxt="__('Last Name')" frname="last_name" :valinf="old('last_name')" :notify="$errors->get('last_name')" required autocomplete />

                <x-forms.input :lbltxt="__('E-mail')" frname="email" :valinf="old('email')" :notify="$errors->get('email')" required autocomplete />

                <x-forms.input :lbltxt="__('Password')" frname="password" inptyp="password" :valinf="old('password')" :notify="$errors->get('password')" required
                    autocomplete />

                <x-forms.input :lbltxt="__('Confirm Password')" frname="password_confirmation" inptyp="password" :notify="$errors->get('password_confirmation')" required
                    autocomplete />

                <x-forms.button btnico="userplus">{{ __('Register') }}</x-forms.button>

                <div class="form-links">
                    @if (Route::has('login'))
                        <x-helpers.link :route="route('login')">{{ __('Already registered? Log in') }}</x-helpers.link>
                    @endif
                    @if (Route::has('password.request'))
                        <x-helpers.link route="{{ route('password.request') }}">{{ __('Recover your password') }}</x-helpers.link>
                    @endif
                </div>

            </x-forms.holder>

            <div class="footer"></div>
        </section>

        <section>
            <x-helpers.cover />
        </section>
    </div>
</x-access-layout>
