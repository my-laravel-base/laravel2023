<?php

namespace App\Http\Controllers\Common;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Retrieve the XML from gitlab
     */
    public function index(): View
    {
        return view('pages.landing.index');
    }
}
