<?php

namespace App\Http\Controllers\Common;

use Redirect;
use Carbon\Carbon;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class ChangelogController extends Controller
{
    /**
     * Retrieve the XML from gitlab
     */
    public function index()
    {
        if (config('app.github_url') != null) {
            $chnglgtagsurl = config('app.github_url') . config('app.github_tags');
            $chnglgcommurl = config('app.github_url') . config('app.github_commits');
            $chnglguserurl = config('app.github_url') . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR;
            $github = true;
        } else {
            $chnglgtagsurl = config('app.gitlab_url') . config('app.gitlab_tags');
            $chnglgcommurl = config('app.gitlab_url') . config('app.gitlab_commits');
            $chnglguserurl = config('app.gitlab_url') . DIRECTORY_SEPARATOR . 'users?active=true&name=';
            $github = false;
        }

        // Get all the tags api information
        $changelog = Http::get($chnglgtagsurl);
        if ($changelog->successful()) {
            // Add extra info
            $gitinfo = null;
            foreach ($changelog->json() as $idx => $item) {
                if ($github) {
                    // Get commit details
                    $commitlog = Http::get($chnglgcommurl . DIRECTORY_SEPARATOR . $item['commit']['url']);
                } else {
                    // Get commit details
                    $commitlog = Http::get($chnglgcommurl . DIRECTORY_SEPARATOR . $item['commit']['id']);

                    // Get commit user
                    $commitusr = Http::get($chnglguserurl . "'" . $item['commit']['author_name'] . "'");
                }

                // Build info
                $gitinfotmp['status']       = ($github)
                    ? null
                    : $commitlog['status'];
                $gitinfotmp['project_id']   = ($github)
                    ? null
                    : $commitlog['project_id'];
                $gitinfotmp['tag']          = (object) ['name' => $item['name']];
                $gitinfotmp['commit']       = (object) [
                    'title'                     => ($github)
                        ? $commitlog['message']
                        : $commitlog['title'],

                    'message'                   => $commitlog['message'],

                    'author_avatar'             => ($github)
                        ? $commitusr['avatar_url']
                        : $commitusr[0]['avatar_url'],

                    'author_name'               => ($github)
                        ? $commitlog['commit']['author']['name']
                        : $commitlog['author_name'],

                    'author_email'              => ($github)
                        ? $commitlog['commit']['author']['email']
                        : $commitlog['author_email'],

                    'committed_date'            => ($github)
                        ? Carbon::parse($commitlog['commit']['committer']['date'])->format('M d, Y - H:i')
                        : Carbon::parse($commitlog['committed_date'])->format('M d, Y - H:i'),

                    'web_url'                   => ($github)
                        ? $commitlog['html_url']
                        : $commitlog['web_url'],

                    'additions'                 => $commitlog['stats']['additions'],

                    'deletions'                 => $commitlog['stats']['deletions']
                ];
                $gitinfo[$idx]              = (object) $gitinfotmp;
            }

            // Display
            return view('pages.landing.changelog', ['changelog' => collect($gitinfo)]);
        } else {
            // Error
            return Redirect::route('dashboard')
                ->with('warning', 'There is a <strong>problem</strong> trying to retrieve the <strong>changelog information</strong>. Please, try again later.');
        }
    }
}
