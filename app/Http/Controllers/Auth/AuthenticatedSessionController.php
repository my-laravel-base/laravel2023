<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\LoginRequest;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('pages.auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        /* **** Log action **** */
        Log::channel('loginactions')
            ->info(Str::upper(Auth::user()->username) . " | New login.");
        /* **** Log action **** */

        // If the user has not been updated, after login redirect to profile edit screen
        if (empty(Auth::user()->profile->date_of_birth) && empty(Auth::user()->profile->gender)) {
            // If the user does not have a complete profile, send it to update it.
            return redirect(route('profile.edit', ['user' => Auth::user()]))
                ->with('info', '<strong>Your profile is incomplete!</strong><br />Please provide the required missing information before continuing.<br /><em>Date of Birth</em> & <em>Gender</em> are the ones needed.<br />Thank you!');
        } else {
            // All good. Redirect home.
            return redirect()->intended(RouteServiceProvider::HOME);
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        /* **** Log action **** */
        Log::channel('loginactions')
            ->info(Str::upper(Auth::user()->username) . " | Logout.");
        /* **** Log action **** */

        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
