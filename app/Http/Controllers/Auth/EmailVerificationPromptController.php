<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Providers\RouteServiceProvider;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     */
    public function __invoke(Request $request): RedirectResponse|View
    {
        if (!$request->user()->hasVerifiedEmail()) {
            return view('pages.auth.verify-email');
        } else {
            if (empty(Auth::user()->profile->date_of_birth) && empty(Auth::user()->profile->gender)) {
                // If the user does not have a complete profile, send it to update it.
                return redirect(route('profile.edit', ['user' => Auth::user()]))
                    ->with('info', '<strong>Your profile is incomplete!</strong><br />Please provide the required missing information before continuing.<br /><em>Date of Birth</em> & <em>Gender</em> are the ones needed.<br />Thank you!');
            } else {
                // All good. Redirect home.
                return redirect()->intended(RouteServiceProvider::HOME);
            }
        }
    }
}
