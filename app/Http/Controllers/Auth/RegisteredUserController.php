<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Users\UserRequest;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('pages.auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(UserRequest $request): RedirectResponse
    {
        // Check if it's initial user
        $initUsr = (User::count()) ? false : true;

        // Pre-fill values with validated info, default role and create user
        $user    = User::create($request->validated());
        $profile = $user->profile->create($request->validated());
        $role    = ($initUsr)
            ? $user->role->whereName(config('defaults.full.role'))->first()
            : $user->role->whereName(config('defaults.basic.role'))->first();

        // Create the registered user
        if ($user->update(['profile_id' => $profile->id, 'role_id' => $role->id])) {
            // Dispatch event
            event(new Registered($user));

            // Log the user
            Auth::login($user);

            /* **** Log action **** */
            Log::channel('application')
                ->info("REGISTRATION | New User: " . Str::upper(Auth::user()->username));
            /* **** Log action **** */

            if (empty(Auth::user()->profile->date_of_birth) && empty(Auth::user()->profile->gender)) {
                // If the user does not have a complete profile, send it to update it.
                return redirect(route('profile.edit', ['user' => Auth::user()]))
                    ->with('info', '<strong>Your profile is incomplete!</strong><br />Please provide the required missing information before continuing.<br /><em>Date of Birth</em> & <em>Gender</em> are the ones needed.<br />Thank you!');
            } else {
                // All good. Redirect home.
                return redirect(RouteServiceProvider::HOME);
            }
        }
    }
}
