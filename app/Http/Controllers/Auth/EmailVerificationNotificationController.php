<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Providers\RouteServiceProvider;

class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification.
     */
    public function store(Request $request): RedirectResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            if (empty(Auth::user()->profile->date_of_birth) && empty(Auth::user()->profile->gender)) {
                // If the user does not have a complete profile, send it to update it.
                return redirect(route('profile.edit', ['user' => Auth::user()]))
                    ->with('info', '<strong>Your profile is incomplete!</strong><br />Please provide the required missing information before continuing.<br /><em>Date of Birth</em> & <em>Gender</em> are the ones needed.<br />Thank you!');
            } else {
                // All good. Redirect home.
                return redirect()->intended(RouteServiceProvider::HOME);
            }
        }

        $request->user()->sendEmailVerificationNotification();

        return back()->with('status', 'verification-link-sent');
    }
}
