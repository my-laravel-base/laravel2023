<?php

namespace App\Http\Controllers\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\AccessControlLevel\Role;
use App\Models\AccessControlLevel\Permission;
use App\Http\Requests\AccessControlLevel\RoleRequest;

class RoleController extends Controller
{
    /**
     * Create the controller instance.
     */
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $roles = Role::query()->orderBy('name')->paginate(10);
        return view('pages.acl.roles-index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RoleRequest $request)
    {
        // Create a new role
        $newRole = new Role;
        $newRole->name = $request->validated('name');
        $newRole->slug = Str::slug($request->validated('name'), '-');
        $newRole->save();

        // Attach default Permission
        $default_permition_list = Permission::query()->whereIn('rule', config('defaults.basic.permissions'))->pluck('id');
        $newRole->permissions()->attach($default_permition_list->all());

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Role: " . $newRole->name . " | Created by: " . Auth::user()->username);
        /* **** Log action **** */

        // Redirect to edit profile
        return redirect(route('acl.roles.edit', ['role' => $newRole]))
            ->with('info', '<strong>"' . $newRole->name . '" ACL profile</strong> created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        $permissions = Permission::query()->orderBy('name')->get();
        return view('pages.acl.roles-edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RoleRequest $request, Role $role)
    {
        // Update the role name / slug
        $role->update([
            'name'          => $request->validated('name'),
            'slug'          => Str::slug($request->validated('name'), '-'),
            'updated_at'    => now(),
        ]);

        // Enable / Disable selected permissions
        $role->permissions()->sync($request->validated('permissions'));

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Role: " . $role->name . " | Edited by: " . Auth::user()->username);
        /* **** Log action **** */

        // Return
        return redirect(route('acl.roles.index'))
            ->with('success', '<strong>"' . $role->name . '" ACL profile</strong> updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        $deleted = $role->name;

        // Do not detach the permissions so we can recover the role in the future
        // $rmvprms = $role->permissions->pluck('id');
        // $role->permissions()->detach($rmvprms);

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Role: " . $role->name . " | Deleted by: " . Auth::user()->username);
        /* **** Log action **** */

        // Detach permissions from role and remove role.
        if ($role->delete()) {
            return redirect(route('acl.roles.index'))
                ->with('warning', '<strong>"' . $deleted . '" ACL profile</strong> deleted.');
        }
    }
}
