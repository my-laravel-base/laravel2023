<?php

namespace App\Http\Controllers\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\AccessControlLevel\Role;
use App\Models\AccessControlLevel\Permission;
use App\Http\Requests\AccessControlLevel\PermissionRequest;

class PermissionController extends Controller
{
    /**
     * Create the controller instance.
     */
    public function __construct()
    {
        $this->authorizeResource(Permission::class, 'permission');
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $permissions = Permission::query()->orderBy('name')->paginate(10);
        return view('pages.acl.perm-index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PermissionRequest $request)
    {
        // Create a new permission
        $newPermission = new Permission;
        $newPermission->name = $request->validated('name');
        $newPermission->rule = Str::slug($request->validated('name'), '-');
        $newPermission->save();

        // Always attach the new permission to the superadmin role
        $superole = Role::whereSlug(config('defaults.full.role'))->first();
        $superole->permissions()->attach($newPermission);

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Permission: " . $newPermission . " | Created by: " . Auth::user()->username);
        /* **** Log action **** */

        // Redirect to edit profile
        return redirect(route('acl.perm.edit', ['permission' => $newPermission]))
            ->with('info', '<strong>"' . $newPermission->name . '" ACL permission</strong> created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Permission $permission)
    {
        return view('pages.acl.perm-edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PermissionRequest $request, Permission $permission)
    {
        // Update the role name / slug
        $permission->update([
            'name'          => $request->validated('name'),
            'rule'          => Str::slug($request->validated('name'), '-'),
            'updated_at'    => now(),
        ]);

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Permission: " . $permission->name . " | Edited by: " . Auth::user()->username);
        /* **** Log action **** */

        // Return
        return redirect(route('acl.perm.index'))
            ->with('success', '<strong>"' . $permission->name . '" ACL permission</strong> updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Permission $permission)
    {
        $deleted = $permission->name;

        // Detach the permissions from the role if it gets removed
        $dtchrole = $permission->roles()->pluck('id');
        $permission->roles()->detach($dtchrole);

        /* **** Log action **** */
        Log::channel('useractions')
            ->info("ACL | Permission: " . $deleted . " | Deleted by: " . Auth::user()->username);
        /* **** Log action **** */

        // Detach permissions from role and remove role.
        if ($permission->delete()) {
            return redirect(route('acl.perm.index'))
                ->with('warning', '<strong>"' . $deleted . '" ACL permission</strong> deleted.');
        }
    }
}
