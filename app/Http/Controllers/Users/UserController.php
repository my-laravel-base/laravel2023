<?php

namespace App\Http\Controllers\Users;

use Illuminate\View\View;
use App\Models\Users\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Users\UserRequest;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Create the controller instance.
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }


    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $users = User::query()->paginate(10);
        return view('pages.users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Page title
        $title = Str::title('Add a new user');

        // Show the page
        return view('pages.users.profile.create', ['pgtl' => $title]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {
        // Pre-fill values with validated info
        $newUser = User::query()->create($request->validated());
        $newProfile = $newUser->profile()->create($request->validated());
        $newUser->update(['profile_id' => $newProfile->id]);

        // Check if file submitted & update
        if (isset($request->safe()->avatar)) {
            // Set new values
            $newAvatarName = Str::random(10) . time() . Str::random(10) . '.' . $request->avatar->getClientOriginalExtension();
            $newUserAvatarDir = 'users' . DIRECTORY_SEPARATOR . $newUser->username;
            $mainAvatarDir = Storage::disk('private');

            if (!$mainAvatarDir->directoryExists($newUserAvatarDir)) {
                // If the directory does not exist, create it
                $mainAvatarDir->createDirectory($newUserAvatarDir);
            } else {
                // If the directory exist, remove the old profile images in it
                $mainAvatarDir->delete($mainAvatarDir->allFiles($newUserAvatarDir));
            }

            // Save the new file
            $mainAvatarDir->putFileAs($newUserAvatarDir, $request->safe()->avatar, $newAvatarName);

            /* **** Log action **** */
            Log::channel('useractions')
                ->info(Str::upper(Auth::user()->username) . " | Has uploaded the user profile image " . $newUserAvatarDir . DIRECTORY_SEPARATOR . $newAvatarName);
            /* **** Log action **** */

            // Update the user profile
            $newUser->profile->avatar = DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . $newUser->username . DIRECTORY_SEPARATOR . 'avatar' . DIRECTORY_SEPARATOR . $newAvatarName;
        }

        // Update info
        if ($newUser->save() && $newUser->profile->save()) {

            /* **** Log action **** */
            Log::channel('useractions')
                ->info(Str::upper(Auth::user()->username) . " | Has created the user profile for user " . Str::upper($newUser->username));
            /* **** Log action **** */

            return Redirect::route('user.list')
                ->with('success', '<strong>User profile</strong> updated successfully.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): View
    {
        // Page title
        if (empty($user->profile->first_name) && empty($user->profile->last_name)) {
            $title = Str::title($user->username);
        } else {
            $title = Str::title($user->profile->full_name);
        }

        /* **** Log action **** */
        Log::channel('useractions')
            ->info(Str::upper(Auth::user()->username) . " | It's on profile edit view for user " . Str::upper($user->username));
        /* **** Log action **** */

        // Show the page
        return view('pages.users.profile.edit', ['pgtl' => $title, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, User $user)
    {
        // If email changes
        if ($request->safe()->email != $user->email) {
            $user->email_verified_at = null;
        }

        // Pre-fill values with validated info
        $user->fill($request->validated());

        // Pre-fill the profile
        $user->profile->fill($request->validated());

        // Check if file submitted & update
        if (isset($request->safe()->avatar)) {
            // Set new values
            $newAvatarName = Str::random(10) . time() . Str::random(10) . '.' . $request->avatar->getClientOriginalExtension();
            $userAvatarDir = 'users' . DIRECTORY_SEPARATOR . $user->username;
            $mainAvatarDir = Storage::disk('private');

            if (!$mainAvatarDir->directoryExists($userAvatarDir)) {
                // If the directory does not exist, create it
                $mainAvatarDir->createDirectory($userAvatarDir);
            } else {
                // If the directory exist, remove the old profile images in it
                $mainAvatarDir->delete($mainAvatarDir->allFiles($userAvatarDir));
            }

            // Save the new file
            $mainAvatarDir->putFileAs($userAvatarDir, $request->safe()->avatar, $newAvatarName);

            /* **** Log action **** */
            Log::channel('useractions')
                ->info(Str::upper(Auth::user()->username) . " | Has uploaded the user profile image " . $userAvatarDir . DIRECTORY_SEPARATOR . $newAvatarName);
            /* **** Log action **** */

            // Update the user profile
            $user->profile->avatar = DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . $user->username . DIRECTORY_SEPARATOR . 'avatar' . DIRECTORY_SEPARATOR . $newAvatarName;
        }

        // Update info
        if ($user->save() && $user->profile->save()) {

            /* **** Log action **** */
            Log::channel('useractions')
                ->info(Str::upper(Auth::user()->username) . " | Has updated the user profile for user " . Str::upper($user->username));
            /* **** Log action **** */

            return Redirect::route('user.list')
                ->with('success', '<strong>User profile</strong> updated successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(UserRequest $request, User $user)
    {
        // User to be deleted
        $username   = $user->username;
        $userID     = $user->id;

        // Reset account
        $user->update(['email_verified_at' => null]);
        $user->profile->update(['avatar' => null]);

        // Delete the user
        if ($user->delete()) {

            // Remove the profile
            $user->profile->delete();

            // remove profile images
            $userAvatarDir = 'users' . DIRECTORY_SEPARATOR . $username;
            $mainAvatarDir = Storage::disk('private');
            $mainAvatarDir->deleteDirectory($userAvatarDir);

            if ($userID == Auth::user()->id) {
                // re-stablish session
                $request->session()->invalidate();
                $request->session()->regenerateToken();

                /* **** Log action **** */
                Log::channel('loginactions')
                    ->info(Str::upper(Auth::user()->username) . " | Logout.");
                /* **** Log action **** */

                /* **** Log action **** */
                Log::channel('application')
                    ->info(Str::upper(Auth::user()->username) . " | Has deleted the user " . Str::upper($username));
                /* **** Log action **** */

                // Back to home.
                return Redirect::route('landing');
            } else {
                // Back
                return Redirect::route('user.list');
            }
        }
    }


    /**
     * This method provides the secured and private avatar
     * for the user in review
     *
     * @param String $avatar
     * @return void
     */
    public function serve(User $user, $avatar = null)
    {
        $avatarDownload = 'users' . DIRECTORY_SEPARATOR . $user->username . DIRECTORY_SEPARATOR . $avatar;
        if (Storage::disk('private')->exists($avatarDownload)) {
            return Storage::disk('private')->download($avatarDownload);
        } else {
            return abort(404);
        }
    }
}
