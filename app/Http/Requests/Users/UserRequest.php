<?php

namespace App\Http\Requests\Users;

use App\Models\Users\User;
use App\Models\Users\Profile;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\File;
use App\Models\AccessControlLevel\Role;
use Illuminate\Validation\Rules\Password;
use App\Http\Requests\CustomBaseFormRequest;

class UserRequest extends CustomBaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // User ACL
        if (Auth::guest()) {
            return true;
        } else if (Auth::user()->username === $this->user()->username) {
            return true;
        } else {
            return match ($this->method()) {
                default         => Auth::user()->HasPermissionTo('view-a-list-of-users') ||
                    Auth::user()->HasPermissionTo('view-a-user') ||
                    Auth::user()->HasPermissionTo('view-a-list-of-users-profiles') ||
                    Auth::user()->HasPermissionTo('view-a-user-profile'),

                'POST'          => Auth::user()->HasPermissionTo('create-a-user') ||
                    Auth::user()->HasPermissionTo('restore-a-user') ||
                    Auth::user()->HasPermissionTo('create-a-user-profile') ||
                    Auth::user()->HasPermissionTo('restore-a-user-profile'),

                'PUT', 'PATCH'  => Auth::user()->HasPermissionTo('update-a-user') ||
                    Auth::user()->HasPermissionTo('update-a-user-profile'),

                'DELETE'        => Auth::user()->HasPermissionTo('delete-a-user') ||
                    Auth::user()->HasPermissionTo('force-delete-a-user') ||
                    Auth::user()->HasPermissionTo('delete-a-user-profile') ||
                    Auth::user()->HasPermissionTo('force-delete-a-user-profile'),
            };
        }
    }


    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        // We receive the role id as string from select.
        if (is_string($this->role_id)) {
            // Check if the user can change roles
            if (Auth::user()->HasPermissionTo('change-a-users-role')) {
                $newRoleID = Role::whereSlug($this->role_id)->first();
                //  Change it to ID and then validate.
                $this->merge(['role_id' => $newRoleID->id]);
            }
        }
    }


    /**
     * Set the validation rules that apply to the POST request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function store(): array
    {
        return [
            /* **** These items belong to user model **** */
            'username'          => ['required', 'string', 'max:64', Rule::unique(User::class, 'username')],
            'email'             => ['required', 'string', 'email:rfc,dns', 'max:152'],
            'password'          => ['required', 'confirmed', Password::defaults()],
            'profile_id'        => ['nullable', 'integer', Rule::unique(Profile::class, 'id')],
            'role_id'           => ['nullable', 'integer', Rule::exists(Role::class, 'id')],

            /* **** These items belong to profile model **** */
            'first_name'        => ['required', 'string', 'max:64'],
            'middle_name'       => ['nullable', 'string', 'max:64'],
            'last_name'         => ['required', 'string', 'max:64'],
            'avatar'            => [
                'nullable',
                File::image()->min(10)->max(8 * 512)->dimensions(Rule::dimensions()->maxWidth(1280)->maxHeight(1280))
            ],
            'date_of_birth'     => ['nullable', 'date:m/d/Y'],
            'gender'            => ['nullable', 'string', 'max:16'],
            'language'          => ['nullable', 'string', 'max:2'],
            'social_security'   => ['nullable', 'string', 'max:16'],
        ];
    }


    /**
     * Set the validation rules that apply to the PUT / PATCH request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function update(): array
    {
        return [
            /* **** These items belong to user model **** */
            'username'          => ['string', 'nullable', 'max:64', Rule::unique(User::class)->ignore($this->user()->id)],
            'email'             => ['string', 'required', 'email:rfc,dns', 'max:152'],
            'profile_id'        => [Rule::unique(Profile::class)->ignore($this->user()->profile->id)],
            'role_id'           => ['nullable', 'integer', Rule::exists(Role::class, 'id')],

            /* **** These items belong to profile model **** */
            'first_name'        => ['string', 'required', 'max:64'],
            'middle_name'       => ['string', 'nullable', 'max:64'],
            'last_name'         => ['string', 'required', 'max:64'],
            'avatar'            => [
                'nullable',
                File::image()->min(10)->max(8 * 512)->dimensions(Rule::dimensions()->maxWidth(1280)->maxHeight(1280))
            ],
            'date_of_birth'     => ['required', 'date:Y-m-d'],
            'gender'            => ['required', 'string', 'max:16'],
            'language'          => ['nullable', 'string', 'max:2'],
            'social_security'   => ['nullable', 'string', 'max:16'],
        ];
    }


    /**
     * Set the validation rules that apply to the DELETE request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function delete(): array
    {
        return [
            'password'          => ['required', 'current_password'],
        ];
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            /* **** These items belong to user model **** */
            'username'          => '<strong>username</strong>',
            'email'             => '<strong>email address</strong>',
            'profile_id'        => '<strong>profile</strong>',
            'role_id'           => '<strong>role type</strong>',
            'password'          => '<strong>password</strong>',

            /* **** These items belong to profile model **** */
            'first_name'        => '<strong>first name</strong>',
            'middle_name'       => '<strong>middle name</strong>',
            'last_name'         => '<strong>last name</strong>',
            'avatar'            => '<strong>profile image</strong>',
            'date_of_birth'     => '<strong>date of birth</strong>',
            'gender'            => '<strong>gender</strong>',
            'language'          => '<strong>language</strong>',
            'social_security'   => '<strong>social security</strong>',

            /* **** These items belong to role model **** */
            'role_type'         => '<strong>roles</strong>',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'password.current_password' => 'You <strong>cannot delete the account</strong> if the <strong>password entered</strong> is incorrect.',
        ];
    }
}
