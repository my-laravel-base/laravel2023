<?php

namespace App\Http\Requests\AccessControlLevel;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CustomBaseFormRequest;
use App\Models\AccessControlLevel\Permission;

class PermissionRequest extends CustomBaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // User ACL
        return match ($this->method()) {
            default         => Auth::user()->HasPermissionTo('view-a-list-of-permissions') ||
                Auth::user()->HasPermissionTo('view-a-permission'),

            'POST'          => Auth::user()->HasPermissionTo('create-a-permission') ||
                Auth::user()->HasPermissionTo('restore-a-permission'),

            'PUT', 'PATCH'  => Auth::user()->HasPermissionTo('update-a-permission'),

            'DELETE'        => Auth::user()->HasPermissionTo('delete-a-permission') ||
                Auth::user()->HasPermissionTo('force-delete-a-permission'),
        };
    }


    /**
     * Set the validation rules that apply to the POST request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function store(): array
    {
        return [
            'name'          => ['string', 'required', 'max:128', Rule::unique(Permission::class)],
            'rule'          => ['string', 'nullable', 'max:128',],
        ];
    }


    /**
     * Set the validation rules that apply to the PUT / PATCH request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function update(): array
    {
        return [
            'name'          => ['string', 'required', 'max:128', Rule::unique(Permission::class)->ignore($this->permission->id)],
            'rule'          => ['string', 'nullable', 'max:128',],
        ];
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'name'          => '<strong>permission name</strong>',
            'rule'          => '<strong>rule</strong>',
        ];
    }
}
