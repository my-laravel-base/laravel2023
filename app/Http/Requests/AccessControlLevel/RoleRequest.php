<?php

namespace App\Http\Requests\AccessControlLevel;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\AccessControlLevel\Role;
use App\Http\Requests\CustomBaseFormRequest;

class RoleRequest extends CustomBaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        // User ACL
        return match ($this->method()) {
            default         => Auth::user()->HasPermissionTo('view-a-list-of-roles') ||
                Auth::user()->HasPermissionTo('view-a-role'),

            'POST'          => Auth::user()->HasPermissionTo('create-a-role') ||
                Auth::user()->HasPermissionTo('restore-a-role'),

            'PUT', 'PATCH'  => Auth::user()->HasPermissionTo('update-a-role'),

            'DELETE'        => Auth::user()->HasPermissionTo('delete-a-role') ||
                Auth::user()->HasPermissionTo('force-delete-a-role'),
        };
    }


    /**
     * Set the validation rules that apply to the POST request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function store(): array
    {
        return [
            /* **** These items belong to role model **** */
            'name'          => ['string', 'required', 'max:128', Rule::unique(Role::class)],
            'slug'          => ['string', 'nullable', 'max:128',],

            /* **** These items belong to permissions model **** */
            'permissions'   => ['array'],
        ];
    }


    /**
     * Set the validation rules that apply to the PUT / PATCH request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function update(): array
    {
        return [
            /* **** These items belong to role model **** */
            'name'          => ['string', 'required', 'max:128', Rule::unique(Role::class)->ignore($this->role->id)],
            'slug'          => ['string', 'required', 'max:128',],

            /* **** These items belong to permissions model **** */
            'permissions'   => ['array'],
        ];
    }


    /**
     * Set the validation rules that apply to the DELETE request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function delete(): array
    {
        return [
            'slug'          => ['string', 'required', 'max:128', Rule::exists(Role::class)],
        ];
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            /* **** These items belong to role model **** */
            'name'          => '<strong>new role name</strong>',
            'slug'          => '<strong>slug</strong>',

            /* **** These items belong to permissions model **** */
            'permissions'   => '<strong>permissions</strong>',
        ];
    }
}
