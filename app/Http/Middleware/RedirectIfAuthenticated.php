<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if (empty(Auth::user()->profile->date_of_birth) && empty(Auth::user()->profile->gender)) {
                    // If the user does not have a complete profile, send it to update it.
                    return redirect(route('profile.edit', ['user' => Auth::user()]))
                        ->with('info', '<strong>Your profile is incomplete!</strong><br />Please provide the required missing information before continuing.<br /><em>Date of Birth</em> & <em>Gender</em> are the ones needed.<br />Thank you!');
                } else {
                    // All good. Redirect home.
                    return redirect(RouteServiceProvider::HOME);
                }
            }
        }

        return $next($request);
    }
}
