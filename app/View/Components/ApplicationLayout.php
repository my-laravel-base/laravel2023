<?php

namespace App\View\Components;

use Illuminate\View\View;
use Illuminate\View\Component;

class ApplicationLayout extends Component
{
    public $title;


    /**
     * Initiate the component
     *
     * @param String $title
     */
    public function __construct($title = null)
    {
        $this->title = $title;
    }


    /**
     * Get the view / contents that represents the component.
     */
    public function render(): View
    {
        return view('layouts.application');
    }
}
