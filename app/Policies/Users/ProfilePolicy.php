<?php

namespace App\Policies\Users;

use App\Models\Users\User;
use App\Models\Users\Profile;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class ProfilePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Auth::user()->HasPermissionTo('view-any-user-profile');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Profile $profile): bool
    {
        return Auth::user()->HasPermissionTo('view-user-profile');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Auth::user()->HasPermissionTo('create-user-profile');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Profile $profile): bool
    {
        return Auth::user()->HasPermissionTo('update-user-profile');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Profile $profile): bool
    {
        return Auth::user()->HasPermissionTo('delete-user-profile');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Profile $profile): bool
    {
        return Auth::user()->HasPermissionTo('restore-user-profile');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Profile $profile): bool
    {
        return Auth::user()->HasPermissionTo('force-delete-user-profile');
    }
}
