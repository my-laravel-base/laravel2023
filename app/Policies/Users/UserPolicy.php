<?php

namespace App\Policies\Users;

use App\Models\Users\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return Auth::user()->HasPermissionTo('view-a-list-of-users');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user): bool
    {
        return Auth::user()->HasPermissionTo('view-a-user');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return Auth::user()->HasPermissionTo('create-a-user');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user): bool
    {
        return (Auth::user()->username === $user->username) || Auth::user()->HasPermissionTo('update-a-user');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user): bool
    {
        return Auth::user()->HasPermissionTo('delete-a-user');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user): bool
    {
        return Auth::user()->HasPermissionTo('restore-a-user');
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user): bool
    {
        return Auth::user()->HasPermissionTo('force-delete-a-user');
    }
}
