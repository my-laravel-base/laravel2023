<?php

namespace App\Models\AccessControlLevel;

use Carbon\Carbon;
use App\Models\AccessControlLevel\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "roles_permissions";


    /**
     * Get the value of the model's route key.
     */
    public function getRouteKey(): mixed
    {
        return $this->rule;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'rule',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'rule',
        'pivot',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Change how the user role's updated time is shown / saved.
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('M d, Y H:i') : null, // Shown
            set: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('Y-m-d H:i:s') : null, // Saved
        );
    }


    /* ******************* */
    /* ***** HELPERS ***** */
    /* ******************* */


    /* ************************* */
    /* ***** RELATIONSHIPS ***** */
    /* ************************* */
    /**
     * This is the direct relantionship between the permissions and the role models.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'roles_permissions_pivot', 'permission_id', 'role_id');
    }
}
