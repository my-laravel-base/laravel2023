<?php

namespace App\Models\AccessControlLevel;

use Carbon\Carbon;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\AccessControlLevel\Permission;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory, SoftDeletes;


    /**
     * Get the value of the model's route key.
     */
    public function getRouteKey(): mixed
    {
        return $this->slug;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'slug',
        'pivot',
        'created_at',
        'deleted_at'
    ];


    /**
     * Change how the user role's created time is shown / saved.
     */
    protected function updatedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('M d, Y H:i') : null, // Shown
            set: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('Y-m-d H:i:s') : null, // Saved
        );
    }


    /* ******************* */
    /* ***** HELPERS ***** */
    /* ******************* */
    /**
     * This retrieves an array with the list of roles. The columns being used to build it are selectable.
     * The default ones are slug for key and name for value, resulting in the following array:
     *  [slug] => name
     *
     * @param string $value The column name that will be used as a value in the array
     * @param string $key   The column name that will be used as a key in the array
     *
     * @return Array
     */
    public function list_all_in_array($value = 'name', $key = 'slug')
    {
        return Role::all()->pluck($value, $key);
    }



    /**
     * This function allows you to know if the current role has the permissions available
     *
     * @param string $rule  The permission to check
     * @return boolean
     */
    public function IsAllowed($rule = 'update_user'): bool
    {
        return $this->permissions->contains('rule', $rule);
    }


    /* ************************* */
    /* ***** RELATIONSHIPS ***** */
    /* ************************* */
    /**
     * This is the direct relantionship between the role and the users models.
     *
     * @return hasMany
     */
    public function users(): hasMany
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }


    /**
     * This is the direct relantionship between the permissions and the role models.
     *
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions_pivot', 'role_id', 'permission_id');
    }
}
