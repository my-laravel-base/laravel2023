<?php

namespace App\Models\Users;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profile extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "users_profiles";


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'date_of_birth',
        'gender',
        'language',
        'social_security',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'created_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date_of_birth'     => 'date',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'user_age'
    ];

    /**
     * Change how the user profile's date of birth is shown / saved.
     */
    protected function dateOfBirth(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('M d, Y') : null, // Shown
            set: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('Y-m-d') : null, // Saved
        );
    }


    /**
     * Change how the user profile's updated time is shown / saved.
     */
    protected function updatedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('M d, Y H:i') : null, // Shown
            set: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('Y-m-d H:i:s') : null, // Saved
        );
    }


    /**
     * This is a helper function that retrieves the full name of the user already formatted
     *
     * @return Attribute
     */
    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn () => $this->processFullName($this->first_name, $this->last_name, $this->middle_name)
        );
    }


    /**
     * This function pre-process and builds the user full name as attribute according to its content.
     *
     * @param String $fname
     * @param String $lname
     * @param String $mname
     *
     * @return String
     */
    private function processFullName($fname = null, $lname = null, $mname = null)
    {
        if (empty($mname)) {
            // No middle name for user
            $userFullName = (!empty($lname) && !empty($fname))
                ? Str::ucfirst($lname) . ', ' . Str::ucfirst($fname)
                : null;
        } else {
            // Middle name included
            $userFullName = (!empty($lname) && !empty($fname))
                ? Str::ucfirst($lname) . ', ' . Str::ucfirst($fname) . ' ' . Str::ucfirst(Str::charAt($mname, 0)) . '.'
                : null;
        }
        return $userFullName;
    }


    /**
     * This is a helper function that retrieves the age of the user already formatted
     *
     * @return Attribute
     */
    protected function userAge(): Attribute
    {
        return new Attribute(
            get: fn () => (!empty($this->date_of_birth)) ? Carbon::parse($this->date_of_birth)->age : ' -- ',
        );
    }


    /* ************************* */
    /* ***** RELATIONSHIPS ***** */
    /* ************************* */
    /**
     * This is the reverse relantionship between the user and the profile models.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'profile_id')->withDefault();
    }
}
