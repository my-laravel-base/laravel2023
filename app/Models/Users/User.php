<?php

namespace App\Models\Users;

use Carbon\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use App\Models\AccessControlLevel\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;


    /**
     * Get the value of the model's route key.
     */
    public function getRouteKey(): mixed
    {
        return $this->username;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'profile_id',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
        'password',
        'remember_token',
        'profile_id',
        'role_id',
        'email_verified_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password'          => 'hashed',
    ];


    /**
     * Change how the user profile's updated time is shown / saved.
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('M d, Y H:i') : null, // Shown
            set: fn ($value) => (!empty($value)) ? Carbon::parse($value)->format('Y-m-d H:i:s') : null, // Saved
        );
    }


    /* ******************* */
    /* ***** HELPERS ***** */
    /* ******************* */
    /**
     * This helper function allows you to know if the logged in user's role, allows you to do certain actions
     * enabled by the permission on it. You need to submit the permission to check to the method.
     *
     * @param string $rule  The name of the permission to check
     * @return boolean      The boolean response let's you know if the permission is included in the user's role
     */
    public function HasPermissionTo($rule = 'view_user'): bool
    {
        return Auth::user()->role->permissions->contains('rule', $rule);
    }


    /* ************************* */
    /* ***** RELATIONSHIPS ***** */
    /* ************************* */
    /**
     * This is the direct relantionship between the user and the role models.
     *
     * @return HasOne
     */
    public function role(): HasOne
    {
        return $this->hasOne(Role::class, 'id', 'role_id')->withDefault();
    }



    /**
     * This is the direct relantionship between the user and the profile models.
     *
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class, 'id', 'profile_id')->withDefault();
    }
}
