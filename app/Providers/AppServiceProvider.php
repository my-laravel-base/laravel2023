<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /* ****************************************************************************** */
        /* **** https://laravel.com/docs/10.x/migrations#index-lengths-mysql-mariadb **** */
        Schema::defaultStringLength(191);
        /* ****************************************************************************** */

        /* ********************************************* */
        /* **** Extending Blade with new directives **** */
        Blade::if('UserCan', function (mixed $rules) {
            // If we receive an array of permissions
            if (is_array($rules)) {
                $idxrules = 0;
                foreach ($rules as $rule) {
                    (Auth::user()->HasPermissionTo($rule)) ? $idxrules++ : $idxrules;
                }
                return ($idxrules) ? true : false;
            }
            // -------------------------------------

            // If we receive only one permission
            return Auth::user()->HasPermissionTo($rules);
            // ---------------------------------
        });
        /* ********************************************* */
    }
}
