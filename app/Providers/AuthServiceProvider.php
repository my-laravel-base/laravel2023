<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Users\User;
use App\Models\Users\Profile;
use App\Policies\Users\UserPolicy;
use App\Policies\Users\ProfilePolicy;
use App\Models\AccessControlLevel\Role;
use App\Models\AccessControlLevel\Permission;
use App\Policies\AccessControlLevel\RolePolicy;
use App\Policies\AccessControlLevel\PermissionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class             => UserPolicy::class,
        Profile::class          => ProfilePolicy::class,
        Role::class             => RolePolicy::class,
        Permission::class       => PermissionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}
