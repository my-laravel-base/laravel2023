<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Common\MainController;
use App\Http\Controllers\Common\ChangelogController;
use App\Http\Controllers\Common\DashboardController;
use App\Http\Controllers\AccessControlLevel\RoleController;
use App\Http\Controllers\AccessControlLevel\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',                                                             [MainController::class, 'index'])->name('landing');

Route::middleware(['auth', 'verified'])->group(function () {
    /**
     * All these routes work only if the user is logged in and it's email is verified
     * https://laravel.com/docs/10.x/routing#route-group-middleware
     */
    Route::get('/dashboard',                                                [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/user/list',                                                [UserController::class, 'index'])->name('user.list');
    Route::get('/user/profile/new',                                         [UserController::class, 'create'])->name('profile.create');
    Route::post('/user/profile/new',                                        [UserController::class, 'store'])->name('profile.store');
    Route::get('/user/profile/edit/{user:username}',                        [UserController::class, 'edit'])->name('profile.edit');
    Route::patch('/user/profile/edit/{user:username}',                      [UserController::class, 'update'])->name('profile.update');
    Route::delete('/user/delete/{user:username}',                           [UserController::class, 'destroy'])->name('user.delete');
    Route::get('/user/{user:username}/avatar/{image}',                      [UserController::class, 'serve']);

    Route::get('/access-control-level/roles',                               [RoleController::class, 'index'])->name('acl.roles.index');
    // Route::get('/access-control-level/roles',                            [RoleController::class, 'create'])->name('acl.roles.create');
    Route::post('/access-control-level/roles',                              [RoleController::class, 'store'])->name('acl.roles.store');
    // Route::get('/access-control-level/roles/{role:slug}',                [RoleController::class, 'show'])->name('acl.roles.show');
    Route::get('/access-control-level/roles/{role:slug}/edit',              [RoleController::class, 'edit'])->name('acl.roles.edit');
    Route::patch('/access-control-level/roles/{role:slug}',                 [RoleController::class, 'update'])->name('acl.roles.update');
    Route::delete('/access-control-level/roles/{role:slug}',                [RoleController::class, 'destroy'])->name('acl.roles.destroy');

    Route::get('/access-control-level/permissions',                         [PermissionController::class, 'index'])->name('acl.perm.index');
    // Route::get('/access-control-level/permissions',                      [PermissionController::class, 'create'])->name('acl.perm.create');
    Route::post('/access-control-level/permissions',                        [PermissionController::class, 'store'])->name('acl.perm.store');
    // Route::get('/access-control-level/permissions/{permission:rule}',    [PermissionController::class, 'show'])->name('acl.perm.show');
    Route::get('/access-control-level/permissions/{permission:rule}/edit',  [PermissionController::class, 'edit'])->name('acl.perm.edit');
    Route::patch('/access-control-level/permissions/{permission:rule}',     [PermissionController::class, 'update'])->name('acl.perm.update');
    Route::delete('/access-control-level/permissions/{permission:rule}',    [PermissionController::class, 'destroy'])->name('acl.perm.destroy');

    Route::get('/system/changes/log',                                       [ChangelogController::class, 'index'])->name('changelog');
});


/**
 * All the Auth routes
 */
require __DIR__ . '/auth.php';


/**
 * Avoiding 404 redirecting to landing
 * https://laravel.com/docs/10.x/routing#fallback-routes
 */
Route::fallback(function ($url) {
    // Log message about this action
    Log::channel('application')
        ->error(
            (auth()->check())
                ? "Avoiding 404: '" . config('app.url') . "/" . $url . "' for user: " . auth()->user()->username . "."
                : "Avoiding 404: '" . config('app.url') . "/" . $url . "' for unknown user."
        );

    // Redirect
    return redirect('/');
});
