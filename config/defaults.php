<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Roles & Permissions for users
    |--------------------------------------------------------------------------
    |
    | These values are the default roles & permissionsof the project. These are
    | cumulative values, so you need to combine them to create the roles. The
    | importance level is from top to bottom. The last role should have the most
    | options for permissions.
    |
    |       basic   -> basic.permissions
    |       complex -> basic.permissions + complex.permissions
    |       full    -> basic.permissions + complex.permissions + full.permissions
    |
    | Check the seeders files for more information on how they are built
    |
    */
    'basic' => [
        'role'          => 'Regular User',
        'permissions'   => [
            /* **** ROLES **** */
            'view-a-list-of-roles'          => 'View a list of roles',
            'view-a-role'                   => 'View a role',
            /* **** PERMISSIONS **** */
            'view-a-list-of-permissions'    => 'View a list of permissions',
            'view-a-permission'             => 'View a permission',
            /* **** USERS **** */
            'view-a-list-of-users'          => 'View a list of users',
            'view-a-user'                   => 'View a user',
            /* **** USERS PROFILES **** */
            'view-a-list-of-users-profiles' => 'View a list of users profiles',
            'view-a-user-profile'           => 'View a user profile',
            /* **** ACTIONS **** */
        ],
    ],

    'complex' => [
        'role'          => 'Administrator',
        'permissions'   => [
            /* **** ROLES **** */
            'create-a-role'                 => 'Create a role',
            'update-a-role'                 => 'Edit / Update a role',
            /* **** PERMISSIONS **** */
            'create-a-permission'           => 'Create a permission',
            'update-a-permission'           => 'Edit / Update a permission',
            /* **** USERS **** */
            'create-a-user'                 => 'Create a user',
            'update-a-user'                 => 'Edit / Update a user',
            /* **** USERS PROFILES **** */
            'create-a-user-profile'         => 'Create a user profile',
            'update-a-user-profile'         => 'Edit / Update a user profile',
            /* **** ACTIONS **** */
            'view-other-users'              => 'View other users',
            'view-other-users-profiles'     => 'View other users profiles',
            'View detailed changelog'       => 'view-detailed-changelog',
        ],
    ],

    'full' => [
        'role'          => 'Super Administrator',
        'permissions'   => [
            /* **** ROLES **** */
            'delete-a-role'                 => 'Delete a role',
            'restore-a-role'                => 'Restore a role',
            'force-delete-a-role'           => 'Force-delete a role',
            /* **** PERMISSIONS **** */
            'delete-a-permission'           => 'Delete a permission',
            'restore-a-permission'          => 'Restore a permission',
            'force-delete-a-permission'     => 'Force-delete a permission',
            /* **** USERS **** */
            'delete-a-user'                 => 'Delete a user',
            'restore-a-user'                => 'Restore a user',
            'force-delete-a-user'           => 'Force-delete a user',
            /* **** USERS PROFILES **** */
            'delete-a-user-profile'         => 'Delete a user profile',
            'restore-a-user-profile'        => 'Restore a user profile',
            'force-delete-a-user-profile'   => 'Force-delete a user profile',
            /* **** ACTIONS **** */
            'change-a-users-role'           => 'Change a user\'s role',
            'update-other-users'            => 'Update other users',
            'update-other-users-profiles'   => 'Update other users profiles',
        ],
    ],
];
