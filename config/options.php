<?php

return [

    /*
    |--------------------------------------------------------------------------
    | List of fixed values for select options
    |--------------------------------------------------------------------------
    |
    | This file is for storing the values of the different select options that
    | do not require constant update or are fixed for common selections.
    | Do not include dynamic values or selects that require constant changes!
    |
    */

    'gender' => [
        ''          => 'Select an Option',
        'male'      => 'Male',
        'female'    => 'Female',
        'none'      => 'Unspecified',
    ],

    'language' => [
        ''          => 'Select an Option',
        'en'        => 'English',
        'es'        => 'Spanish',
        'fr'        => 'French',
    ],
];
