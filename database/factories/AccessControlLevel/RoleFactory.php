<?php

namespace Database\Factories\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AccessControlLevel\Role>
 */
class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $role = Str::title(fake()->jobTitle());
        return [
            'name' => $role,
            'slug' => Str::slug($role, '-'),
        ];
    }
}
