<?php

namespace Database\Factories\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AccessControlLevel\Permission>
 */
class PermissionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $rule = Str::title(fake()->jobTitle());
        return [
            'name' => $rule,
            'rule' => Str::replace(' ', '-', Str::lower($rule)),
        ];
    }
}
