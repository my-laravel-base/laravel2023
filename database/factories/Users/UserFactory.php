<?php

namespace Database\Factories\Users;

use Illuminate\Support\Str;
use Database\Factories\Users\ProfileFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\AccessControlLevel\RoleFactory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Users\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $created = fake()->dateTimeBetween('-1 years', '-8 months');
        $updated = fake()->dateTimeBetween('-8 months', '-10 days');

        return [
            'username'          => fake()->username(),
            'email'             => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'    => Str::random(10),
            'profile_id'        => ProfileFactory::new(['created_at' => $created, 'updated_at' => $updated]),
            'role_id'           => RoleFactory::new(),
            'created_at'        => $created,
            'updated_at'        => $updated,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
