<?php

namespace Database\Factories\Users;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Users\Profile>
 */
class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // User Gender
        $gender     = fake()->randomElement(['male', 'female']);

        // First name
        $firstName  = ($gender == 'male') ? fake()->firstNameMale() : fake()->firstNameFemale();

        // Middle name can be null
        $middleName = ($gender == 'male')
            ? fake()->randomElement([null, fake()->firstNameMale()])
            : fake()->randomElement([null, fake()->firstNameFemale()]);

        // Language
        $language   = fake()->randomElement(['en', 'es', 'fr']);

        return [
            'first_name'        => $firstName,
            'middle_name'       => $middleName,
            'last_name'         => fake()->lastName(),
            'date_of_birth'     => fake()->dateTimeBetween('-90 years', '-15 months')->format('Y-m-d'),
            'gender'            => $gender,
            'language'          => $language,
            'social_security'   => null,
            'avatar'            => null,
        ];
    }
}
