<?php

namespace Database\Seeders\Users;

use Str;
use App\Models\Users\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run($role1 = null, $role2 = null, $role3 = null): void
    {
        // Create the superadmin
        User::factory()->create([
            'username'          => Str::lower(config('defaults.full.role')),
            'email'             => 'contact@support.com',
            'password'          => '$2y$10$KyUYxg6er6LNOPiriSoNk.gTA8Ft.bLAPDM31Y.Y1YRXgZ/zPZQNe',
            'role_id'           => $role3->id,
        ]);

        // Create the admin
        User::factory()->create([
            'username'          => Str::lower(config('defaults.complex.role')),
            'email'             => 'contact@support.com',
            'password'          => '$2y$10$iIrRFaxQGC1z7f5o7vRfxem9vNc0zqKuay774L.AuATbcySpraBA.',
            'role_id'           => $role2->id,
        ]);

        // Create a regular user
        User::factory()->create([
            'username'          => 'juanmcortez',
            'email'             => 'juanm.cortez@gmail.com',
            'password'          => '$2y$10$1vfcqpg7V99yI3h6xt8.Y.3QaVORHUKkRqaEHLS6nEYmhM2FoIG12',
            'role_id'           => $role1->id,
        ]);
    }
}
