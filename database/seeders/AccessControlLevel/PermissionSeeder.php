<?php

namespace Database\Seeders\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Database\Seeders\Users\UserSeeder;
use App\Models\AccessControlLevel\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run($role1 = null, $role2 = null, $role3 = null): void
    {
        // Full list of basic permissions array
        $permissions = config('defaults.basic.permissions');
        // Create & assign all the base permissions
        foreach ($permissions as $rule => $description) {
            $currentRule = Permission::factory()->create([
                'name' => $description,
                'rule' => $rule,
            ]);
            $role1->permissions()->attach($currentRule);
            $role2->permissions()->attach($currentRule);
            $role3->permissions()->attach($currentRule);
        }

        // Full list of complex permissions array
        $permissions = config('defaults.complex.permissions');
        // Create & assign all the complex permissions
        foreach ($permissions as $rule => $description) {
            $currentRule = Permission::factory()->create([
                'name' => $description,
                'rule' => $rule,
            ]);
            $role2->permissions()->attach($currentRule);
            $role3->permissions()->attach($currentRule);
        }

        // Full list of full permissions array
        $permissions = config('defaults.full.permissions');
        // Create & assign all the full permissions
        foreach ($permissions as $rule => $description) {
            $currentRule = Permission::factory()->create([
                'name' => $description,
                'rule' => $rule,
            ]);
            $role3->permissions()->attach($currentRule);
        }

        // Create the initial users.
        $roles = ['role1' => $role1, 'role2' => $role2, 'role3' => $role3];
        $this->callOnce(UserSeeder::class, false, $roles);
    }
}
