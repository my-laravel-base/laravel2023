<?php

namespace Database\Seeders\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\AccessControlLevel\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Database\Seeders\AccessControlLevel\BasicPermissionSeeder;

class BasicRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Create the base roles
        $basicRole = Role::factory()->create([
            'name'  => config('defaults.full.role'),
            'slug'  => Str::slug(config('defaults.full.role')),
        ]);


        // Create the initial permissions.
        $roles = ['basicRole' => $basicRole];
        $this->callOnce(BasicPermissionSeeder::class, true, $roles);
    }
}
