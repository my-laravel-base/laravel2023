<?php

namespace Database\Seeders\AccessControlLevel;

use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use App\Models\AccessControlLevel\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BasicPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run($basicRole = null): void
    {
        // Full list of basic permissions array
        $permissions = Arr::collapse(
            [
                config('defaults.basic.permissions'),
                config('defaults.complex.permissions'),
                config('defaults.full.permissions')
            ]
        );

        // Sort them
        $permissions = Arr::sort($permissions);

        // Assing it
        foreach ($permissions as $rule => $description) {
            $currentRule = Permission::factory()->create([
                'name' => $description,
                'rule' => $rule,
            ]);
            $basicRole->permissions()->attach($currentRule);
        }
    }
}
