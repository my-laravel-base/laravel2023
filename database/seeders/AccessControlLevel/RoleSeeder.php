<?php

namespace Database\Seeders\AccessControlLevel;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\AccessControlLevel\Role;
use Database\Seeders\AccessControlLevel\PermissionSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Create the base roles
        $role1 = Role::factory()->create([
            'name'  => config('defaults.basic.role'),
            'slug'  => Str::slug(config('defaults.basic.role')),
        ]);


        $role2 = Role::factory()->create([
            'name'  => config('defaults.complex.role'),
            'slug'  => Str::slug(config('defaults.complex.role')),
        ]);


        $role3 = Role::factory()->create([
            'name'  => config('defaults.full.role'),
            'slug'  => Str::slug(config('defaults.full.role')),
        ]);


        // Create the initial permissions.
        $roles = ['role1' => $role1, 'role2' => $role2, 'role3' => $role3];
        $this->callOnce(PermissionSeeder::class, true, $roles);
    }
}
