<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles_permissions_pivot', function (Blueprint $table) {
            $table->foreignId('role_id')
                ->constrained('roles', 'id')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('permission_id')
                ->constrained('roles_permissions', 'id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles_permissions_pivot');
    }
};
