<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_profiles', function (Blueprint $table) {
            $table->id();

            $table->string('first_name', 64)->nullable();
            $table->string('middle_name', 64)->nullable();
            $table->string('last_name', 64)->nullable();

            $table->date('date_of_birth')->nullable();
            $table->string('gender', 16)->nullable();
            $table->string('language', 2)->nullable()->default('en');
            $table->string('social_security', 16)->nullable();

            $table->longText('avatar')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_profiles');
    }
};
